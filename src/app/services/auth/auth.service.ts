import {Injectable} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import 'rxjs/Rx';
import {Observable} from 'rxjs';
import { apiRoutes } from "../../config/config";

// decorator
@Injectable()
export class AuthService {
    private isUserLoggedIn = false;
    constructor(private http: Http) {

    }

    signup(content: any) {
        const body = JSON.stringify(content);
        // I am using X-Requested-With header to tell Laravel that it is an ajax call although it is not necessary but it is a good practice to use it.
        const header = new Headers({
            'Content-Type': 'application/json',
            'X-Requested-With': 'XMLHttpRequest'
        });
        return this.http.put(apiRoutes.auth.signUp, body, {
            headers: header
        })
            .map(
            (response: Response) => response.json()
            );
    }

    decodeToken(token: string) {
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        return JSON.parse(window.atob(base64));
    }


    /**
    * Function to login user and set session token or JWT token in localstorage.
    *
    **/
    signin(email: string, password: string) {
        // I am using X-Requested-With header to tell Laravel that it is an ajax call although it is not necessary but it is a good practice to use it.
        const header = new Headers({
            'Content-Type': 'application/json',
            'X-Requested-With': 'XMLHttpRequest'
        });
        return this.http.post(apiRoutes.auth.logIn,
            {email: email, password: password},
            {headers: header}
        )
            .do(tokenData => {
                //                console.log('0');
                //                localStorage.setItem('token', tokenData.token)
            })
            .map((response: Response) => {
                let data = response.json();
                if (data.status) {
                    const token = data._JWTtoken;
                    // set token in localstorage for future use
                    localStorage.setItem('_uims_token', token);
                    this.isUserLoggedIn = true;
                    return {status: true, message: '', token: token, decoded: this.decodeToken(token)};
                } else {
                    return {status: false, message: data.errors};
                }
            });
    }

    getToken() {
        return localStorage.getItem('_uims_token') || null;
    }

    deletToken() {
        return (localStorage.removeItem('_uims_token')) ? true : false;
    }

    public get_projects(){console.log("service called");
        return this.http.get(apiRoutes.project.getAllProjects).map((response: Response) => {
                return response.json();
            }
        )
    }
    
    public getUserLoggedIn(){
        if(this.getToken() && this.getToken() !== null && this.getToken() != "" ){
            return true;
        }else{
            return false;
        }
        
    }

    public user_logout(token:any){

        return this.http.get(apiRoutes.auth.logOut+token).map((response: Response) => {return response.json()});    
    }

}