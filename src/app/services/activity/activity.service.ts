import { Injectable} from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/Rx';
import  { Observable } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { apiRoutes } from "../../config/config";


@Injectable()
export class ActivityService {
    public header:any;
    constructor(private http: Http, private authService: AuthService) {
        this.header = new Headers({
            'Content-Type': 'application/json',
            'Authorization' : 'Bearer ' + this.authService.getToken()
        });
    }

    public register_task_activity(activityData:any) {
        return this.http.post(apiRoutes.activity.newTaskActivity, JSON.stringify(activityData), {headers: this.header}).map((response: Response) => response.json());
    }

}