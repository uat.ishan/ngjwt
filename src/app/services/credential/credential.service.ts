import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from "rxjs/Rx";
import { AuthService } from '../auth/auth.service';
import { apiRoutes } from '../../config/config';


@Injectable()
export class CredentialService {

	public header:any;
	constructor(private http: Http, private authService: AuthService) { 
		this.header = new Headers({
            'Content-Type': 'application/json',
            'Authorization' : 'Bearer ' + this.authService.getToken()
        }); 
	}

	/* Function to save new task in the database added on a specific status board */
	public add_new_credential(credential:any){

		return this.http.post(apiRoutes.credential.saveCredential, JSON.stringify(credential), {headers: this.header}).map((response: Response) => response.json());

	}

	public get_project_credential(project_id){

		return this.http.get(apiRoutes.credential.getCredential+project_id, {headers: this.header}).map((response: Response) => response.json());

	}

}
