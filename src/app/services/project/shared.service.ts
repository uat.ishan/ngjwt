import {Component, Injectable, Input, Output, EventEmitter} from '@angular/core'


@Injectable()
export class sharedService {
    @Output() currentProject: EventEmitter<any> = new EventEmitter();
    @Output() dataChangeObserver: EventEmitter<any> = new EventEmitter();
    data  = "";
    constructor() {
        // console.log('shared service started');
    }

    change() {
        console.log('change started');
        this.currentProject.emit(true);
    }

    getEmittedValue() {
        return this.currentProject;
    }

    get_project() {
        return this.currentProject;
    }

    setData(data: any) {
        this.data = data;
        this.dataChangeObserver.emit(this.data);
        return this.dataChangeObserver;
    }

    set_project(project:any){
        this.currentProject.emit(project);
    }


} 
