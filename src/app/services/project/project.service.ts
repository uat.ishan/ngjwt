import { Injectable, Pipe, PipeTransform } from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import 'rxjs/Rx';
import {Observable} from 'rxjs';
import {AuthService} from '../auth/auth.service';
import { apiRoutes } from '../../config/config';

/* pipe filter to filter project in project main list */
// @Pipe({
//     name: 'searchfilter',
//     pure: false
// })

@Injectable()
export class ProjectService {

    public header: any;
    constructor(private http: Http, private authService: AuthService) {
        this.header = new Headers({
            'Content-Type': 'application/json',
            'Authorization' : 'Bearer ' + this.authService.getToken()
        });
    }

    /* API call to save projects */
    public save_project(projectDetails: {}) {
        const header = new Headers({
            'Content-Type': 'application/json',
            'Authorization' : 'Bearer ' + this.authService.getToken()
        });
        return this.http.put(apiRoutes.project.addNewProject, JSON.stringify(projectDetails), {headers: header}).map((response: Response) => {
            return response.json();
        });
    }

    /* API call to function to get all projects of current user using userId */
    public get_projects() {
         const header = new Headers({
            'Content-Type': 'application/json',
            'Authorization' : 'Bearer ' + this.authService.getToken()
        });
        return this.http.get(apiRoutes.project.getAllProjects, {headers: header}).map((response: Response) => {
            return response.json();
        });
    }

    public get_single_project(handleName:string){
        
        /* Api call to get single project of handle from database */
        return this.http.get(apiRoutes.project.getSingleProject+handleName, {headers: this.header}).map((response: Response) => response.json());        
    }

    public update_project(projectData:any){

        /* API call to update project in database passing parameters*/
        var newProjectData = { project_id:projectData.projectId, name:projectData.projectName,handle:projectData.projectHandle, category:projectData.projectCategory,privacy_status:projectData.privacyStatus, user_id:projectData.projectAssignee };
        return this.http.patch(apiRoutes.project.updateProject+projectData.projectId, JSON.stringify(newProjectData), {headers: this.header}).map((response: Response) => response.json());        
    
    }

    public delete_assignee(assigneeDetails:any){
        return this.http.post(apiRoutes.project.deleteAssignee, JSON.stringify(assigneeDetails), {headers: this.header}).map((response: Response) => response.json());
    }

    public delete_project(projectId:any){
        return this.http.delete(apiRoutes.project.deleteProject+projectId, {headers: this.header}).map((response: Response) => response.json());
    }

}