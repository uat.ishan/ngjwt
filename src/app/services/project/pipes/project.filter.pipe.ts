import {Injectable, Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'projectfilter'
})
@Injectable()
export class ProjectFilterPipe implements PipeTransform {
    transform(projects: any, search: string): any {
        if (!projects || !search) {
            return projects;
        }
        // filter items array, items which match and return true will be kept, false will be filtered out
        return projects.filter(item => item.name.toLowerCase().indexOf(search.toLowerCase()) !== -1);
    }
}