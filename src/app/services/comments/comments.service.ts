import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from "rxjs/Rx";
import { AuthService } from '../auth/auth.service';
import { apiRoutes } from '../../config/config';


@Injectable()
export class CommentsService {

	public header:any;
	constructor(private http: Http, private authService: AuthService) { 
		this.header = new Headers({
            'Content-Type': 'application/json',
            'Authorization' : 'Bearer ' + this.authService.getToken()
        }); 
	}

	/* Function to save new task in the database added on a specific status board */
	public add_new_comment(commentDetails:any){

		var commentData = {type: commentDetails.type, user_id:commentDetails.userId, message:commentDetails.message, source_id:commentDetails.sourceID};
		return this.http.post(apiRoutes.comment.saveComment, JSON.stringify(commentData), {headers: this.header}).map((response: Response) => response.json());

	}

	/* Function to get task from database regarding type and dataId */
	public get_comments(commentData:any){

		return this.http.post(apiRoutes.comment.getTaskComments, JSON.stringify({type:commentData.type, source_id:commentData.dataId, days:commentData.days}), {headers: this.header}).map((response: Response) => response.json());

	}

}
