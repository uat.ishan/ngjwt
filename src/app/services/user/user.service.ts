import {Injectable} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import 'rxjs/Rx';
import  { Observable } from 'rxjs';
import {AuthService} from '../auth/auth.service';
import { apiRoutes } from "../../config/config";


@Injectable()
export class UserService {
    public header:any;
    constructor(private http: Http, private authService: AuthService) {
        this.header = new Headers({
            'Content-Type': 'application/json',
            'Authorization' : 'Bearer ' + this.authService.getToken()
        });
    }

    addUser(content: string) {
        console.log(content);
        const body = JSON.stringify({
            content: content,
        });
        // I am using X-Requested-With header to tell Laravel that it is an ajax call although it is not necessary but it is a good practice to use it.
        const header = new Headers({
            'Content-Type': 'application/json',
            'X-Requested-With': 'XMLHttpRequest',
            'Authorization' : 'Bearer ' + this.authService.getToken()
        });
        return this.http.put(apiRoutes.user.addNewUser, body, {
            headers: header
        })
            .map(
            (response: Response) => response.json()
            );
    }

    get_users(){
        return this.http.get(apiRoutes.user.getAllUsers,{ headers: this.header }).map((response: Response) => response.json());        
    }

    public update_user(userDetails){

        /* Api call to update user details */
        var userDetailsData = {first_name:userDetails.firstName, last_name:userDetails.lastName, email:userDetails.email };
        return this.http.patch(apiRoutes.user.updateUser, JSON.stringify(userDetailsData), {headers: this.header}).map((response: Response) => response.json());
    }

    public get_user(){
        
		return this.http.get(apiRoutes.user.getSingleUser,{headers: this.header}).map((response: Response) => response.json());    
	}
	
    deleteUser(id: number) {
        // return this.http.delete('http://localhost:8000/api/user/' + id)
    }

    public check_user_password(currentPassword:any){
        return this.http.post(apiRoutes.user.checkPassword,JSON.stringify({password:currentPassword}), {headers: this.header}).map((response: Response) => response.json());
    }

    public change_password(userData:any){
        
        return this.http.post(apiRoutes.user.changePassword,JSON.stringify({current_password:userData.password, password:userData.new, password_confirmation:userData.confirm }), {headers: this.header}).map((response: Response) => response.json());
    }

    public reset_password(userData:any){
        return this.http.post(apiRoutes.user.newPassword,JSON.stringify({email:userData.email, password:userData.password, password_confirmation:userData.confirmPassword}), {headers: this.header}).map((response: Response) => response.json());
    }

    public forgot_password(userData:any){
                
        return this.http.post(apiRoutes.user.forgotPassword,JSON.stringify(userData), {headers: this.header}).map((response: Response) => response.json());
    }

    public getEmailResetPassword(token:any){
        
        return this.http.get(apiRoutes.user.resetPassword+token, {headers: this.header}).map((response: Response) => response.json());
    }
}