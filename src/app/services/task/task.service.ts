import {Injectable} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import {Observable} from "rxjs/Rx";
import {AuthService} from '../auth/auth.service';
import {apiRoutes} from '../../config/config';


@Injectable()
export class TaskService {

    public header: any;
    constructor(private http: Http, private authService: AuthService) {
        this.header = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.authService.getToken()
        });
    }

    /* Function to save new task in the database added on a specific status board */
    public add_new_task(taskDetails: any) {
        var taskData = {name: taskDetails.taskName, tag_name: taskDetails.taskTags, description: taskDetails.taskDescription, project_id: taskDetails.projectId, status: taskDetails.status, user_id: taskDetails.user, end_date: taskDetails.endDate};
        return this.http.put(apiRoutes.task.addNewTask, JSON.stringify(taskData), {headers: this.header}).map((response: Response) => response.json());

    }

    /* Function to update task in database when dragged from one status to another */
    public update_task_status(taskData: any) {

        let taskDetails = {task_id: parseInt(taskData.currentTaskId), status: parseInt(taskData.newStatusId), currentOrder: taskData.current_task_order, finalOrder: taskData.final_task_order, projectId: taskData.project_id};

        /* Get type API call to update status of task in database */
        return this.http.post(apiRoutes.task.updateTaskStatus, JSON.stringify(taskDetails), {headers: this.header}).map((response: Response) => response.json());

    }

    public add_new_status_board(statusBoardData: any) {

        /* API call to add new status in database passing status_name, user_id, status_id in parameters*/
        var statusData = {status_name: statusBoardData.statusboardName, user_id: 1, status_id: 1, project_id: statusBoardData.projectId};
        return this.http.put(apiRoutes.status.addNewStatus, JSON.stringify(statusData), {headers: this.header}).map((response: Response) => response.json());

    }

    public update_task_details(taskData: any) {

        /* API call to add new status in database passing status_name, user_id, status_id in parameters*/
        var taskDetailsData = {name: taskData.taskName, tag_name: taskData.taskTags, end_date: taskData.endDate, description: taskData.taskDescription, user_id: taskData.user};
        return this.http.patch(apiRoutes.task.updateTaskDetails + taskData.taskId, JSON.stringify(taskDetailsData), {headers: this.header}).map((response: Response) => response.json());

    }

    public get_all_tasks(projectId) {

        /* Api call to get all tasks from database */
        return this.http.get(apiRoutes.task.getAllTasks + projectId, {headers: this.header}).map((response: Response) => response.json());
    }

    public get_single_task(taskId: any) {

        /* Api call to get all tasks from database */
        return this.http.get(apiRoutes.task.updateTaskDetails + taskId, {headers: this.header}).map((response: Response) => response.json());
    }

    public get_all_status(handleName: string) {

        /* Api call to get all status from database */
        return this.http.get(apiRoutes.status.getAllStatus + handleName + '/statuses', {headers: this.header}).map((response: Response) => response.json());
    }

    public delete_task(taskId) {
        return this.http.delete(apiRoutes.task.deleteTask + taskId, {headers: this.header}).map((response: Response) => response.json());
    }

}
