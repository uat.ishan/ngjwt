import {Injectable} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import 'rxjs/Rx';
import {Observable} from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import {NotificationService, NotificationType, NotificationOptions} from '../../lbd/services/notification.service';


/**
*
* This is a common service which holds all the common function to be used on any component 
* To use this you just need to import it in any component.
*
**/


// decorator
@Injectable()
export class CommonService {
    private project = new BehaviorSubject<any>(null);
    private projectIndex = new BehaviorSubject<any>(null);
    public projectName:string;

    constructor(private http: Http, private notificationService: NotificationService) {
        this.projectName = "";
    }

    // fucntion to show error message of an ajax response
    showAjaxErrors(content: any) {
        let message = "";
        if ($.type(content) == 'object' || $.type(content) == 'array') {
            $.each(content, function(k,v){
                message += v + '</br>';
            });
        }else if($.type(content) == 'string' || $.type(content) == 'number'){
            message = content;
        }else{
            console.info('Message must be a string, object or array.');
            return true;
        }
        // this function is in lbd/services/notification.service 
        this.notificationService.notify(new NotificationOptions({
            message: message,
            icon: 'pe-7s-less',
            type: <NotificationType>(4),
            from: 'top',
            align: 'right'
        }));
    }

    // fucntion to show success message of an ajax response
    showAjaxSuccess(content: any) {
        this.notificationService.notify(new NotificationOptions({
            message: content,
            icon: 'pe-7s-check',
            type: <NotificationType>(2),
            from: 'top',
            align: 'right'
        }));
    }

    /* function will be called from controller to get changed value of project */
    get_project(){
        return this.project.asObservable();
    }

    /* Function will be called from controller when project details will be updated */
    set_project(projectDetails:any, index:any){
        var project = [];
        project[index] = {
            projectId:projectDetails.id,
            projectName:projectDetails.name, 
            projectHandle:projectDetails.handle
        };
        this.project.next(project);
    }

    set_project_index(index){
        this.projectIndex.next(index);
    }

    get_project_index(){
        return this.projectIndex.asObservable();
    }

}