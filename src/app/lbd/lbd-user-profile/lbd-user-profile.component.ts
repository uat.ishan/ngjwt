import {Component, Input, ChangeDetectionStrategy, OnInit} from '@angular/core';
import { apiRoutes } from '../../config/config';
declare var $: any;
declare var Dropzone: any

@Component({
    selector: 'lbd-user-profile',
    templateUrl: './lbd-user-profile.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class LbdUserProfileComponent implements OnInit {
    @Input()
    backgroundImage: string;

    @Input()
    avatarImage: string;

    @Input()
    name: string;

    @Input()
    username: string;

    // @Input()
    // about: string;

    constructor() { 

    }

    ngOnInit() {
        /* Function that will initialise dropzone */
        this.initialise_dropzone();
    }

    initialise_dropzone(){

        /* Autodiscover false prevents dropzone from showing error when page is refrersh after once intialisation of dropzpne*/
        // Dropzone.autoDiscover = false;

        // /* Dropzone function with required parameters */
        // var userImage = new Dropzone("#userImage", { 
        //     url: apiRoutes.user.updateUser,
        //     method:'patch',
        //      //  
        //     maxFiles:1, 
        //      headers: {
        //         'Cache-Control': null,
        //         'X-Requested-With': null
        //         // to be sent along with the request
        //     },

        //     sending:function(file, xhr, formData){
        //        // formData.append('someParameter[image]', file);
        //        //  formData.append('someParameter[userName]', 'bob');
        //     },
            
        //     // addRemoveLinks:true,
        //     init: function() {
        //         this.on("maxfilesexceeded", function(file) {
        //             this.removeAllFiles();
        //             this.addFile(file);
        //         });
        //     },
        //     thumbnail: function(file, dataUrl) {
        //         $("._user_image").attr("src",dataUrl);
        //     },
        //     success:function(response){
        //         console.log(response);
        //     },
        //     error:function(error){
        //         console.log(error);
        //     }

        // });

    }

}
