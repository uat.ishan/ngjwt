import {Component, OnInit, Input, ViewChildren, QueryList} from '@angular/core';
import {NavItem} from '../lbd.module';
import {ProjectService} from '../../services/project/project.service';
import {ProjectFilterPipe} from '../../services/project/pipes/project.filter.pipe';
import {CommonService} from '../../services/common/common.service';
import {AuthService} from '../../services/auth/auth.service';
import {Router, NavigationStart, NavigationEnd} from '@angular/router';
import {sharedService} from '../../services/project/shared.service';

declare var $: any;

@Component({
    selector: 'lbd-project-items',
    templateUrl: './lbd-project-items.component.html',
    styleUrls: ['./lbd-project-items.component.css'],
})

export class LbdProjectItemsComponent implements OnInit {

    @Input()
    navItems: NavItem[];

    @Input()
    navbarClass: string;

    @Input()
    showTitles: boolean;

    public projectDetails = {projectName: ""};
    public projects = [];
    public index: any;
    public dummy: string;
    public projectStatus: boolean = false;


    constructor(private router: Router, private _ss: sharedService, private _authService: AuthService, private _projectService: ProjectService, private _commonService: CommonService) {

        if (localStorage.getItem('_uims_token') !== null)
            this.get_all_projects();

    }

    ngOnInit() {
        var self = this;
        $("body").on("click","._project_handle",function(){
            let projectData = JSON.parse($(this).attr("data-project")); 
            self.router.navigate(["dashboard/project",projectData.handle]);
        });
    }

    save_project(event: any) {
        event.stopPropagation();

        /* Variable that will contain project details that need to be send in project service */
        let projectDetails = {
            name: this.projectDetails.projectName,
            category: "default",
            privacy_status: 1
        };

        /* If project name field is not empty only then call save project api */
        if (this.projectDetails.projectName !== "") {

            /* Call to service function that will save project in database */
            this._projectService.save_project(projectDetails).subscribe(
                response => {
                    if (response.status) {
                        this.projectDetails.projectName = "";
                        this._commonService.showAjaxSuccess(response.message);
                        this.create_project_list(response.project);
                        // this.projects.push(response.project);
                        // $("body").trigger("click");
                    }
                    else {
                        if (typeof response.errors === "object") {
                            for (var errorKey in response.errors) {
                                this._commonService.showAjaxErrors(response.errors[errorKey][0]);
                            }
                        }
                        else {
                            this._commonService.showAjaxErrors(response.message);
                        }
                    }
                },
                error => {
                    console.log(error)
                },
            );
        }
        else {
            this._commonService.showAjaxErrors("Please Enter Project Name");
        }
        return false;

    }

    get_all_projects() {
        let self = this;
        this._projectService.get_projects().subscribe(
            response => {
                if (response.status) {
                    $.each(response.projects, function (key, value) {
                        let li = $("<li>",{
                            class:"_projects",
                            style:"cursor:pointer"
                        });
                        let menuComponent = $("<a>",{
                            class:"_project_handle",
                            "data-project":JSON.stringify({"handle":value.handle,"id":value.id,"createdBy":value.created_by}),
                            "data-id":value.id,
                            text:value.name
                        });
                        $("body").find("._project_list_div").append(li.append(menuComponent));
                    });
                    self.projectStatus = true;
                    this._ss.get_project().subscribe(
                        project => {
                            let projectData = $.parseJSON(project);
                            let menuItem = $("._project_handle[data-id='" + projectData.id + "']");
                            this.index = menuItem.attr("data-index");
                            if (projectData.delete_status !== "undefined" && projectData.delete_status) {
                                $("body").find("._project_handle[data-id='" + projectData.projectId + "']").parent("li").remove();
                            } else {
                                menuItem.attr("data-id","").attr("data-id",projectData.id);
                                menuItem.attr("data-project","").attr("data-project",JSON.stringify({"handle":projectData.handle,"id":projectData.id,"createdBy":projectData.created_by}));
                                menuItem.html("").html(projectData.name);
                            }
                        }
                    );
                }
                else {

                }
            },
            error => {
                let self = this;
                if (error.status === 401) {
                    this._commonService.showAjaxErrors("Login Session expired");
                    setTimeout(function () {
                        self.router.navigateByUrl("auth/signout");
                    }, 2000);
                }
            },
        );
    }

    create_project_list(projectDetail: any) {
        $("._project_list_div").append($("<li>", {
            class: "_projects"
        }).append($("<a>", {
            href: window.location.origin + "/dashboard/project/" + projectDetail.handle,
            text: projectDetail.name
        })));
    }


    filterProject(projectName){
        if(projectName ==""){
            $("._project_handle").parent("li").show();
            return false;    
        }
        $("._project_handle").parent("li").hide();
        $("._project_handle").each(function(){
            if($(this).text().toLowerCase().indexOf(projectName.toLowerCase())>-1){
                $(this).parent("li").show();
            }
        })
    }
}
