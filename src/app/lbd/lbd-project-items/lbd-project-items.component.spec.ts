import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LbdProjectItemsComponent } from './lbd-project-items.component';

describe('LbdProjectItemsComponent', () => {
  let component: LbdProjectItemsComponent;
  let fixture: ComponentFixture<LbdProjectItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LbdProjectItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LbdProjectItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
