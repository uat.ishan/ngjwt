import {Component, OnInit} from '@angular/core';
import {FooterItem} from '../lbd/lbd-footer/lbd-footer.component';
import {Http, Response, Headers} from '@angular/http';
import {AuthService} from '../services/auth/auth.service';
import {apiRoutes} from '../config/config';

@Component({
    selector: 'app-main-container-layout',
    templateUrl: './footer-layout.component.html',
    styleUrls: ['./footer.component.css'],
})
export class FooterLayoutComponent implements OnInit {
    public footerItems: FooterItem[];
    public copyright: string;
    public header: any;
    public commitData: string;
    public commitId: string = '';
    public commitMessage: string;

    constructor(private http: Http, private authService: AuthService) {
        this.header = new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.authService.getToken()
        });
    }

    public ngOnInit() {

        this.getLastCommit().subscribe(
            response => {
                if (response.status) {
                    this.commitId = response.commit_data.commit_id;
                    this.commitMessage = response.commit_data.commit_message;
                }
            },
            error => {
                console.log(error);
            },
        );

        this.footerItems = [
            {title: 'Company', routerLink: ''},
            {title: 'Portfolio', routerLink: ''},
            {title: 'Blog', routerLink: ''}
        ];
        this.copyright = '&copy; 2017 UIMS, <a href="http://uimatic.com/">UIMATIC</a>';
    }

    public getLastCommit() {

        /* Api call to get git last commit*/
        return this.http.get(apiRoutes.gitCommit.getLastCommit, {headers: this.header}).map((response: Response) => response.json());
    }
}
