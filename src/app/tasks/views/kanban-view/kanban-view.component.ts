import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, NavigationEnd} from '@angular/router';

//import { NavbarComponent } from './project-nav/navbar/navbar.component';
import {sharedService} from '../../../services/project/shared.service';
import {TaskService} from '../../../services/task/task.service';
import {UserService} from '../../../services/user/user.service';

declare var $: any;

@Component({
    selector: 'app-kanban-view',
    templateUrl: './kanban-view.component.html',
    styleUrls: ['./kanban-view.component.css'],
})
export class KanbanViewComponent implements OnInit {

    // If this variable is true means show add task popup els do not show add task popup
    public showAddTask: boolean = false;
    public assignees: any;

    // This will store all statues or you can say columns of kanban
    public allStatus = [];
    // This will store all the tasks of current project which currently logged in user can view/edit
    public allTasks = [];

    public taskSource: any;

    public taskFields = [
        {name: "id", type: "string"},
        {name: "status", map: "state", type: "string"},
        {name: "text", map: "label", type: "string"},
        {name: "tags", type: "string"},
        {name: "color", map: "hex", type: "string"},
        {name: "resourceId", type: "number"},
        {name: "className", type: "string"},
        {name: "content", type: "string"}
    ];

    public currentProject: any = {
        id: '',
        name: '<div class="_masker"></div>' // this is a mask div which will show a loader untill ajax fetch all necessary data
    };

    public leftMenueItems = [
        {title: '<i class="fa fa-gear"></i>', routerLink: ''},
        {title: 'Credentials', routerLink: ''},
        {title: 'Wiki', routerLink: ''}
    ];
    constructor(private _ss: sharedService, private _taskService: TaskService, private router: Router, private _userService: UserService) {}

    ngOnInit() {
        // Please keep all these class functions in the same order for smooth loading of page
        let self = this;
        // Do R&D on where to place this nonangular js code so that it run in optimized manner may be inside ngAfterContentInit()
        $(".select2").select2();
        $('.datepicker').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
        });
        $('.datepicker').datepicker('setDate', new Date());

        this._ss.get_project().subscribe(
            project => {
                this.currentProject = JSON.parse(project);
            }
        );
        this.router.events.filter(event => event instanceof NavigationEnd).subscribe(route => {
            self.getStatus(route.url.split("/")[3]);
        });

        this.setAllAssignees();


    }

    // Function to get all assignees and set assignees class variable
    setAllAssignees() {
        let self = this;
        this._userService.get_users().subscribe(
            response => {
                if (response.status) {
                    console.log(response);
                    //                    self.assignees = response.users.slice(3, response.users.length);
                    self.assignees = response.users;
                }
                else {
                    if (typeof response.errors === "object") {
                        for (var errorKey in response.errors) {
                            //                            this._commonService.showAjaxErrors(response.errors[errorKey][0]);
                        }
                    }
                    else {
                        //                        this._commonService.showAjaxErrors(response.message);
                    }
                }
            },
            error => {
                console.log(error);
            }
        );
    }

    // Function to show add new task popup and to fill data in popup 
    onShowAddTaskChanged(showAddTask: boolean): void {
        this.showAddTask = showAddTask;
        if (this.showAddTask) {
            $('#taskModal').show();
        }
        this.showAddTask = false;
    }


    /**
     *  Function to set or refresh left menu items
     *  Simply run this function to update left menu item's links or titles when this.currentProject is changed
     */
    setLeftMenu() {
        this.leftMenueItems = [
            {title: '<i class="fa fa-gear"></i>', routerLink: '/dashboard/project/edit/' + this.currentProject.handle},
            {title: 'Credentials', routerLink: '/dashboard/project/credential/' + this.currentProject.handle},
            {title: 'Wiki', routerLink: '/dashboard/project/wiki/' + this.currentProject.handle}
        ];
    }

    /** 
     * Function to get all the status(or columns of kanban) boards from database and store them in a class variable
     * This fucntion also fetch current project from DB and updates it in this.currentProject class variable and all necessagr places by using shared service (_ss.set_project)
     **/
    getStatus(handleName: string) {
        var self = this;
        //        self._progressService.start();
        self.allStatus = [];
        self._taskService.get_all_status(handleName).subscribe(
            response => {
                if (response.status) {
                    console.log(response);
                    this._ss.set_project(JSON.stringify(response.project));
                    this.setLeftMenu();
                    this.getTasks();
                } else {
                    console.log('You dick..!');
                }
            },
            error => console.log(error),
        );
    }

    resourcesAdapterFunc() {

        var resourcesSource = {
            localData: [
                {id: 0},
            ],
            dataType: "array",
            dataFields: [
                {name: "id", type: "number"}
            ]
        };
        var resourcesDataAdapter = new $.jqx.dataAdapter(resourcesSource);
        return resourcesDataAdapter;

    }

    /* Function that will intialise kanban with specified tasks and status */
    create_task_kanban(dataAdapter: any) {
        $('#taskKanBan').jqxKanban('destroy');
//        $("._task_kanban_container").append($("<div>", {
//            id: "taskKanBan"
//        }));
        let self = this;
        $('#taskKanBan').jqxKanban({
            width: '100%',
//            height: ($('.main-content').height() - 10) + 'px',
//            resources: this.resourcesAdapterFunc(),
            source: dataAdapter,
            columns: self.allStatus,
            template: "<div class='jqx-kanban-item' id=''>"
            + "<div style='float:right;' class='fa fa-pencil-square-o edit-task jqx-kanban-item-template-content'></div>"
            + "<div class='jqx-kanban-item-text'></div>"
            + "<div class='jqx-kanban-item-task-description' style='padding:5px;'></div>"
            + "<div style='display: block;' class='jqx-kanban-item-footer'></div>"
            + "</div>",
            itemRenderer: function (element, item, resource) {
//                if (item.content != "New content" && item.content !== null) {
//                    let content = $.parseJSON(item.content);
//                    if (content.description && content.description !== null) {
//                        $(element[0]).find(".jqx-kanban-item-task-description").html(content.description);
//                    }
//                    if (content.endDate && content.endDate !== null) {
//                        // $(element[0]).find(".jqx-kanban-item-task-end-date").html(content.endDate);
//                    }
//                }
            }
        });
//        $("body").find("._no_tags_task").find(".jqx-kanban-item-footer").css("display", "none");
//        $("body").find("._no_access").find(".edit-task").remove();
        //        self.projectOptions = true;
        //        self._progressService.done();
    }


    /**
     * Function to get all tasks related to the current project 
     * It only return task which loggedin user is authorized to view/edit
     */
    getTasks() {
        if (this.currentProject.id) {
            var self = this;
            self.allTasks = [];
            this._taskService.get_all_tasks(self.currentProject.id).subscribe(
                response => {
                    console.log("allTasks");
                    console.log(response);
                    console.log("allTasks");
                    if (response.status) {
                        for (let task of response.tasks) {
                            let taskClasses = (task.tag_name == null) ? "task_" + task.id.toString() + " _no_tags_task" : "task_" + task.id.toString();
                            if (task.edit_check != 1) {
                                taskClasses += " _no_access";
                            }
                            self.allTasks[task.order] = {
                                id: task.id.toString(),
                                state: "data_" + task.status.toString(),
                                label: task.name,
                                tags: (task.tag_name == null) ? " " : task.tag_name,
                                className: taskClasses,
                                hex: "#5dc3f0",
                                resourceId: task.id,
                                createdBy: task.created_by,
                                content: JSON.stringify({
                                    description: task.description,
                                    endDate: (task.end_date == null) ? "" : task.end_date,
                                    createdBy: task.user.first_name + " " + task.user.last_name,
                                    createdAt: task.created_at,
                                }),
                            };
                        }
                        //                        this.register_events();
                    }
                    else {
                        self.allTasks[0] = {
                            id: 0,
                            state: "data_" + 0,
                            label: "",
                            tags: "",
                            className: "task_" + 0,
                            hex: "#5dc3f0",
                            resourceId: 0,
                            content: "",
                        };
                    }
                    self.taskSource = {localData: self.allTasks, dataType: "array", dataFields: self.taskFields};
                    var dataAdapter = new $.jqx.dataAdapter(self.taskSource);
                    this.create_task_kanban(dataAdapter);
                },
                error => console.log(error),
            );
        }
    }


}
