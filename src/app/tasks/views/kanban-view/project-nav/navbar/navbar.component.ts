import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {Router, ActivatedRoute, NavigationStart, NavigationEnd} from '@angular/router';

//Keys with ? means they are optional
export interface NavItem {
    title: string;
    routerLink?: string;
    iconClass?: string;
    onClick?: string; // not working for now
}

@Component({
    selector: 'project-kanban-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

    // need to make these variable dynamic by using attribute binding
    @Input() public projectHandle: string = "project-1-1";
    @Input() public projectName: string = "Project 1";
    @Input() public leftNavItems: NavItem[] = [
        {title: '<i class="fa fa-gear"></i>'},
        {title: 'Credentials', routerLink: '/dashboard/project/credential/' + this.projectHandle},
        {title: 'Wiki', routerLink: '/dashboard/project/wiki/' + this.projectHandle}
    ];
    @Input() public rightNavItems: NavItem[] = [];

    public showAddTask: boolean = false;

    @Output()
    showAddTaskModal: EventEmitter<boolean> = new EventEmitter<boolean>();

    constructor() {}

    ngOnInit(): void {
        let self = this;
    }
    /* call get_status with handle of project to refresh complete kanban */
    refreshKanaban() {
        console.log('Refreshing done...!');
    }

    onAddTask() {
        this.showAddTask = true;
        this.showAddTaskModal.emit(this.showAddTask);
        console.log(this.showAddTask);
    }

}
