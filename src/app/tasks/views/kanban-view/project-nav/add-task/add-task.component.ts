import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'project-kanban-add-task',
    templateUrl: './add-task.component.html',
    styleUrls: ['./add-task.component.css']
})
export class AddTaskComponent implements OnInit {

    @Input() public assignees: any = [];
    @Input() public project: any = {
        id: '',
        name: ''
    };
    
    @Output()
    showAddTaskModal: EventEmitter<boolean> = new EventEmitter<boolean>();
    
    constructor() {}

    ngOnInit() {
    }
    
    onSaveButtonClick(){
        
    }

}
