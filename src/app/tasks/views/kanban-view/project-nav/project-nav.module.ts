import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import {NavbarComponent} from './navbar/navbar.component';
import { AddTaskComponent } from './add-task/add-task.component';
import { KanbanComponent } from './kanban/kanban.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule
    ],
    declarations: [NavbarComponent, AddTaskComponent, KanbanComponent],
    exports: [NavbarComponent, AddTaskComponent, KanbanComponent]
})
export class ProjectNavModule {}
