import {Component, Output, OnInit} from '@angular/core';
import {Router, ActivatedRoute, NavigationEnd} from '@angular/router';
import {TaskService} from '../../../services/task/task.service';
import {CommonService} from '../../../services/common/common.service';
import {UserService} from '../../../services/user/user.service';
import {NgProgressService} from 'ngx-progressbar';
import {sharedService} from '../../../services/project/shared.service';
import {TaskSharedService} from '../../../services/task/task.shared';
import {ActivityService} from '../../../services/activity/activity.service';

declare var $: any;
declare var window: any;

@Component({
    selector: 'app-kanban',
    templateUrl: './kanban.component.html',
    styleUrls: ['./kanban.component.css']
})

export class KanbanComponent implements OnInit {

    @Output()
    public type;
    public userDetail = {id: "", firstName: "", lastName: "", email: "", password: "", confirm: "", new: ""};
    public taskDetails = {taskName: "", taskDescription: "", taskTags: "", taskId: "", status: "", user: "", projectId: 0, endDate: "", createdBy: "", createdAt: ""};
    public statusBoardDetails = {statusboardName: "", projectId: 0};
    public singleTaskAssignee = [];
    public tasks = [];
    public users = [];
    public projectCreator = false;
    public projectOptions = false;
    public allStatus = [];
    public taskSource: any;
    public taskFields = [];
    public projectDetails = {projectId: 0, projectName: "", projectHandle: "", projectCategory: "", projectStatus: 0, projectCreatedBy: ""};
    public taskAssignees = {userId: 0, first_name: ""};
    private subscription;

    constructor(private _progressService: NgProgressService, private _activityService: ActivityService, private _ss: sharedService, private currentActivatedRoute: ActivatedRoute, private _userService: UserService, private _taskService: TaskService, private router: Router, private _commonService: CommonService, private _taskShared: TaskSharedService) {

        window.kanbanRenderStatus = false;
        this.taskFields = [
            {name: "id", type: "string"},
            {name: "status", map: "state", type: "string"},
            {name: "text", map: "label", type: "string"},
            {name: "tags", type: "string"},
            {name: "color", map: "hex", type: "string"},
            {name: "resourceId", type: "number"},
            {name: "className", type: "string"},
            {name: "content", type: "string"}
        ];

        var self = this;

        this._ss.get_project().subscribe(
            project => {

                let projectData = $.parseJSON(project);
                this.projectDetails.projectName = projectData.name;
                this.projectDetails.projectHandle = projectData.handle;

                if (projectData.createdBy == this.userDetail.id) {
                    this.projectCreator = true;
                } else {
                    this.projectCreator = false;
                }

            }
        );

        this._taskShared.get_task().subscribe(
            task => {
                let data = JSON.parse(task);
                this.taskDetails.taskName = data.taskName;
                this.taskDetails.taskDescription = data.taskDescription;
                this.taskDetails.createdBy = data.createdBy;
                this.taskDetails.createdAt = data.createdAt;
                this.taskDetails.endDate = data.endDate;
                this.taskDetails.taskId = data.taskId;
            }
        );
        this.get_user();
    }

    get_user() {
        this._userService.get_user().subscribe(
            response => {
                if (response.status) {
                    this.userDetail.email = response.user.email;
                    this.userDetail.firstName = response.user.first_name;
                    this.userDetail.lastName = response.user.last_name;
                    this.userDetail.id = response.user.id;
                }
                else {

                }
                this._progressService.done();
            },
            error => {console.log(error);}
        );

    }

    /* Function that will be fired when edit project is clicked */
    edit_project() {
        this.router.navigate(["edit/", this.projectDetails.projectHandle], {relativeTo: this.currentActivatedRoute});
    }

    /* Function to be called when add task is clicked to clear all input fields */
    clear_add_task_fields(containerId: string) {
        this.taskDetails = {taskName: "", taskDescription: "", taskTags: "", taskId: "", status: "", user: "", projectId: 0, endDate: "", createdBy: "", createdAt: ""};
        $("._uim_add_task_tags").tagsinput('removeAll');
        $('#' + containerId).find(".task_assignee").val("").trigger("change");
    }

    ngOnInit() {
        var self = this;
        self.type = "tasks";
        self.subscription = self.router.events.filter(event => event instanceof NavigationEnd).subscribe(route => {
            if (!window.viewTask || typeof window.viewTask !== "undefined") {
                self.get_status(route.url.split("/")[3]);
            }
        });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    register_events() {
        var self = this;
        $("input[name='taskTags']").tagsinput({width: '100%'});

        /* API call to userservice to get all users */
        self._userService.get_users().subscribe(
            response => {
                if (response.status) {
                    self.users = response.users.slice(3, response.users.length);
                }
                else {
                    if (typeof response.errors === "object") {
                        for (var errorKey in response.errors) {
                            this._commonService.showAjaxErrors(response.errors[errorKey][0]);
                        }
                    }
                    else {
                        this._commonService.showAjaxErrors(response.message);
                    }
                }
            },
            error => {
                console.log(error);
            }
        );

        $('body').off('itemMoved', '#taskKanBan');
        $('body').on('itemMoved', '#taskKanBan', function (event) {

            /* variable that will contain current task id to update and status value to update */
            let taskData = {currentTaskId: "", newStatusId: "", current_task_order: 0, final_task_order: 0, project_id: self.projectDetails.projectId};
            let args = event.args;
            let newColumn = args.newColumn;
            taskData.newStatusId = $("#" + newColumn.headerElement[0].id).find("._delete_status").attr("class").split(' ')[2].split("_")[1];

            var itemData = args.itemData;

            /* When new task is created at that time class is not added, but when new task is added it is possible to add class */
            if (typeof itemData.className.split("_")[1] !== "undefined") {
                taskData.currentTaskId = itemData.className.split("_")[1];
            }
            else if (typeof itemData.id !== "undefined") {
                taskData.currentTaskId = itemData.id;
            }

            //            self.register_task_activity(event);

            /* get new order of tasks of new status */
            let taskOrder = self.get_task_order(taskData.newStatusId, taskData.currentTaskId);

            taskData.current_task_order = taskOrder.current_order;
            taskData.final_task_order = taskOrder.final_order;

            self._taskService.update_task_status(taskData).subscribe(
                response => {
                    if (response.status) {
                        var updatedTaskData = {event: event, task: response.task}
                        self.refresh_kanaban();
                    }
                    else {
                        if (typeof response.errors === "object") {
                            for (var errorKey in response.errors) {
                                self._commonService.showAjaxErrors(response.errors[errorKey][0]);
                            }
                        }
                        else {
                            self._commonService.showAjaxErrors(response.message);
                        }
                    }
                },
                error => console.log(error),
            );
        });

        //        $('body').off('click', '.edit-task');
        //        $('body').on('click', '.edit-task', function () {
        //            self._progressService.start();
        //            /* Clear all fields of modal */
        //            self.clear_add_task_fields('updateTaskModal');
        //            /* Get task id from class 'task_' where id is set while getting all tasks */
        //            var taskId  = {id:"",status:"edit"};
        //            taskId.id = $(this).parent(".jqx-kanban-item").attr("class").split("task_")[1].split(" ")[0];
        //            
        //            if (typeof self.tasks[taskId.id] !== "undefined" && $(this).parent("div").hasClass("_no_access")) {
        //                self._commonService.showAjaxErrors("Access denied to edit task.");
        //                return false;
        //            }
        //
        //
        //            if (typeof self.tasks[taskId.id] !== "undefined") {
        //                self.taskDetails.taskName = self.tasks[taskId.id].label;
        //                self.taskDetails.taskId = taskId.id;
        //                let content = $.parseJSON(self.tasks[taskId.id].content);
        //                self.taskDetails.taskDescription = content.description;
        //                self.taskDetails.endDate = content.endDate;
        //                $("input[name='newTaskTags']").tagsinput('removeAll');
        //                $("input[name='newTaskTags']").tagsinput('add', self.tasks[taskId.id].tags);
        //                if (typeof self.taskDetails.endDate !== undefined)
        //                    $("#updateEndDate").datepicker('setDate', self.taskDetails.endDate.split(" ")[0]);
        //                /* Code to get single task details and set current assignees of task on asignees select2 */
        //                
        //                self.get_task_assignee(taskId)
        //
        //            }
        //        });
        //
        //        $("body").on("click", "._delete_status", function () {
        //            var statusId = $(this).attr("class").split("status_")[1];
        //            $("input[name='deleteBoardId']").val(statusId);
        //            $("#deleteModal").modal("show");
        //        });
        //
        //        $('body').off('click', '.view-task');
        //        $('body').on('click', '.view-task', function (event) {
        //            self._progressService.start();
        //            /* Get task id from class 'task_' where id is set while getting all tasks */
        //            var taskId = {id:"",status:"show"};
        //            taskId.id = $(this).parent(".jqx-kanban-item").attr("class").split("task_")[1].split(" ")[0];
        //
        //            /* Call to view task details funciton passing "this" element */
        //            self.viewTaskDetails(taskId);
        //            
        //        });
        //
        //        $('#taskDetailModal').on('hidden.bs.modal', function () {
        //            self.router.navigate(["dashboard/project/",self.router.url.split("/")[3]]);
        //        })

    }

    viewTaskDetails(taskId) {
        window.viewTask = true;
        var self = this;

        self.taskDetails.taskName = self.tasks[taskId.id].label;
        self.taskDetails.taskId = taskId.id;
        let content = $.parseJSON(self.tasks[taskId.id].content);
        self.taskDetails.taskDescription = content.description;
        self.taskDetails.endDate = content.endDate;
        if (typeof self.taskDetails.endDate == "undefined")
            self.taskDetails.endDate = "End date is not specified.";

        self.taskDetails.createdBy = content.createdBy;
        self.taskDetails.createdAt = content.createdAt;
        self.taskDetails.taskTags = self.tasks[taskId.id].tags;
        self.get_task_assignee(taskId);
    }

    /* Function that will generate new task order */
    get_task_order(newStatusId, currentTaskId) {

        let container = $(".jqx-kanban-column[data-column-data-field='data_" + newStatusId + "']");
        let childTask = container.find(".jqx-kanban-item");
        let currentOrder = parseInt($("body").find(".task_" + currentTaskId).attr("class").split("order_")[1].split(" ")[0]);
        let orderObj = {current_order: 0, final_order: 0};

        /* Check if its first column in which task is moving */
        if (container.prev(".jqx-kanban-column").length == 0) {

            /* Check if its first task of that status in which it is moving */
            if (childTask.length == 1) {
                orderObj = {
                    current_order: currentOrder,
                    final_order: 1
                }
            }
            else {

                /* If its first status container of project and have multiple tasks in it then only following code executes*/
                if ($("body").find(".task_" + currentTaskId).prev(".jqx-kanban-item").length) {
                    orderObj = {
                        current_order: currentOrder,
                        final_order: parseInt($("body").find(".task_" + currentTaskId).prev(".jqx-kanban-item").attr("class").split("order_")[1].split(" ")[0]) + 1
                    }
                } else {
                    orderObj = {
                        current_order: currentOrder,
                        final_order: parseInt($("body").find(".task_" + currentTaskId).next(".jqx-kanban-item").attr("class").split("order_")[1].split(" ")[0]) - 1
                    }
                }
                if (orderObj.final_order == 0) {
                    orderObj.final_order = 1;
                }
            }
        } else if (container.prev(".jqx-kanban-column").length > 0) {

            /* Traverse each and every column that exist before this column in which task is moved */
            $.each($(".jqx-kanban-column"), function () {

                /* Check only those columns which are before column in which task is moved */
                if (parseInt($(this).attr("id").split("-")[2]) <= parseInt($(container).attr("id").split("-")[2])) {

                    /* Check for child in every column and get last order of the column */
                    if ($(this).find(".jqx-kanban-item").length) {
                        orderObj = {
                            current_order: currentOrder,
                            final_order: parseInt($(this).find(".jqx-kanban-item:last").attr("class").split("order_")[1].split(" ")[0]) + 1
                        }
                    }
                }
            });

            /* After loop on every status if final_order value is 0 that means no task exist in previous statuses, set final_order to 1 */
            if (orderObj.final_order == 0) {
                orderObj.final_order = 1;
            }
        }
        return orderObj;
    }


    /* Function that will register in db activity when task is dragged from one status to other */
    register_task_activity(event) {
        let taskId = parseInt(event.args.itemData.className.split("_")[1]);
        let activtyData = {};
        if (event.args.newColumn.text == event.args.oldColumn.text) {
            activtyData = {
                source_id: taskId,
                type_id: "tasks",
                data: " changes order of " + this.tasks[taskId].label + "'."
            };
        } else {
            activtyData = {
                source_id: taskId,
                type_id: "tasks",
                data: " marked " + this.tasks[taskId].label + " as '" + event.args.newColumn.text + "'."
            };
        }
        this._activityService.register_task_activity(activtyData).subscribe(
            response => {
                console.log(response);
            },
            error => {}
        );
    }

    /* call get_status with handle of project to refresh complete kanban */
    refresh_kanaban() {
        this.get_status(this.router.url.split("/")[3])
    }

    get_task_assignee(taskId) {
        this._taskService.get_single_task(taskId.id).subscribe(
            response => {
                if (response.status) {
                    this.singleTaskAssignee = [];
                    if (response.assigneeUserTask.assigneeUsers[0] != null) {
                        this.singleTaskAssignee = response.assigneeUserTask.assigneeUsers;
                        let asigneeId = [];
                        $(".update_task_assignee").val("");
                        /* Loop to set make array of values and pass it in select2 to set selected value of task asignee */
                        for (let i = 0; i < response.assigneeUserTask.assigneeUsers.length; i++) {
                            asigneeId.push(response.assigneeUserTask.assigneeUsers[i].id);
                            $(".update_task_assignee").val(asigneeId);
                        }
                        $(".update_task_assignee").select2();
                    }
                    if (taskId.status == "show") {
                        $("#taskDetailModal").modal("show");
                        this.router.navigate(['dashboard/project', this.router.url.split("/")[3], taskId.id]);
                    } else {
                        $("#updateTaskModal").modal("show");
                    }

                    this._progressService.done();
                }
                this._taskShared.set_task(JSON.stringify(this.taskDetails))
            },
            error => {}
        );

    }

    ngAfterContentInit() {
        let date = new Date();
        $(".task_assignee").select2({placeholder: "Task Asignees", width: '100%'});
        $(".update_task_assignee").select2({placeholder: "Task Asignees"});
        $('.datepicker').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
        });
        $('#addEndDate').datepicker('setDate', new Date());
    }

    /* Function that will update task details */
    update_task_details() {

        /* Get tags values from taginput */
        this.taskDetails.taskTags = $("input[name='newTaskTags']").val();
        this.taskDetails.projectId = $("body").find("._project_id").val();
        this.taskDetails.endDate = $("#updateEndDate").val();
        if ($("body").find(".update_task_assignee").val() !== null)
            this.taskDetails.user = $("body").find(".update_task_assignee").val().join(",");

        this._taskService.update_task_details(this.taskDetails).subscribe(
            response => {
                if (response.status) {
                    $('#taskKanBan').jqxKanban('updateItem', response.task.id.toString(), {
                        text: response.task.name,
                        content: JSON.stringify({
                            description: response.task.description,
                            endDate: (response.task.end_date == null) ? " " : response.task.end_date,
                            // createdBy:response.task.user.first_name+" "+response.task.user.last_name,
                            createdAt: response.task.created_at,
                        }),
                        tags: (response.task.tag_name == null) ? " " : response.task.tag_name
                    });

                    this.refresh_kanaban();
                    /* Update array that contains all task details */
                    this.tasks[this.taskDetails.taskId] = {
                        id: this.taskDetails.taskId.toString(),
                        state: "data_" + response.task.status.toString(),
                        label: response.task.name,
                        tags: (response.task.tag_name == null) ? " " : response.task.tag_name,
                        className: (response.task.tag_name == null) ? "task_" + response.task.id.toString() + " _no_tags_task" : "task_" + response.task.id.toString() + " order_" + response.task.order,
                        hex: "#5dc3f0",
                        resourceId: response.task.id,
                        createdBy: response.task.created_by,
                        content: JSON.stringify({
                            description: response.task.description,
                            endDate: (response.task.end_date == null) ? " " : response.task.end_date,
                            createdAt: response.task.created_at,
                            taskId: response.task.id,
                        }),
                    }
                }
            },
            error => console.log(error),
        );
        $("#updateTaskModal").modal("hide");
    }

    resourcesAdapterFunc() {

        var resourcesSource = {
            localData: [
                {id: 0},
            ],
            dataType: "array",
            dataFields: [
                {name: "id", type: "number"}
            ]
        };
        var resourcesDataAdapter = new $.jqx.dataAdapter(resourcesSource);
        return resourcesDataAdapter;

    }

    /* Function to get all the status boards from database and store them in a class variable */
    get_status(handleName: string) {
        var self = this;
        self._progressService.start();

        self.allStatus = [];
        self._taskService.get_all_status(handleName).subscribe(
            response => {
                if (response.status) {

                    /* Call to common service function where new project detail is set */
                    self.projectDetails.projectName = response.project.name;
                    self.projectDetails = {projectId: response.project.id, projectName: response.project.name, projectHandle: response.project.handle, projectCategory: response.project.category, projectStatus: response.project.status, projectCreatedBy: response.project.created_by};

                    self._ss.set_project(JSON.stringify({name: response.project.name, handle: response.project.handle, id: response.project.id, createdBy: response.project.created_by}));

                    /* check if login user is creator of project than allow manage project option */
                    self.check_project_creator(self.projectDetails);

                    for (var i = 0; i < response.data.length; i++) {
                        let id = response.data[i].id.toString();
                        let dataFieldValue = "data_" + id;
                        self.allStatus.push({
                            text: response.data[i].status_name,
                            iconClassName: "fa _delete_status status_" + id,
                            dataField: dataFieldValue,
                            collapsible: false
                        });
                    } $("body").find("._project_id").val(response.project.id);
                    self.get_tasks();
                }
                else {
                    if (typeof response.project !== "undefined")
                        $("body").find("._project_id").val(response.project.id);
                    // $('#taskKanBan').jqxKanban('destroy');
                    return false;
                }
            },
            error => console.log(error),
        );
    }

    /* Function that will get all the task stored in the database */

    get_tasks() {

        var self = this;
        self.tasks = [];
        this._taskService.get_all_tasks(self.projectDetails.projectId).subscribe(
            response => {
                if (response.status) {
                    $.each(response.tasks, function (key, task) {

                        let taskClasses = (task.tag_name == null) ? "task_" + task.id.toString() + " _no_tags_task" + " order_" + task.order : "task_" + task.id.toString() + " order_" + task.order;
                        if (task.edit_check != 1) {
                            taskClasses += " _no_access";
                        }
                        self.tasks[task.order] = {
                            id: task.id.toString(),
                            state: "data_" + task.status.toString(),
                            label: task.name,
                            tags: (task.tag_name == null) ? " " : task.tag_name,
                            className: taskClasses,
                            hex: "#5dc3f0",
                            resourceId: task.id,
                            createdBy: task.created_by,
                            content: JSON.stringify({
                                description: task.description,
                                endDate: (task.end_date == null) ? "" : task.end_date,
                                createdBy: task.user.first_name + " " + task.user.last_name,
                                createdAt: task.created_at,
                                taskId: task.id,
                            }),
                        };

                    });
                    console.log(self.tasks);
                    this.register_events();
                }
                else {
                    self.tasks[0] = {
                        id: 0,
                        state: "data_" + 0,
                        label: "",
                        tags: "",
                        className: "task_" + 0,
                        hex: "#5dc3f0",
                        resourceId: 0,
                        content: "",
                    };
                }
                self.taskSource = {localData: self.tasks, dataType: "array", dataFields: self.taskFields};
                var dataAdapter = new $.jqx.dataAdapter(self.taskSource);
                this.create_task_kanban(dataAdapter);
            },
            error => console.log(error),
        );

    }

    /* Function will be called when save is clicked from add task details modal  */
    add_task() {

        if (this.taskDetails.taskName == "") {
            this._commonService.showAjaxErrors("Please enter title for task !!");
        }
        else {
            /* Follwing line of code is to get value from tags input box */
            this.taskDetails.taskTags = $("._uim_add_task_tags").val();
            this.taskDetails.projectId = $("body").find("._project_id").val();
            this.taskDetails.status = $(".jqx-kanban-column:first").attr("data-column-data-field").split("_")[1].toString();
            this.taskDetails.endDate = $("#addEndDate").val();

            if ($("select[name='task_assignee']").val() !== null)
                this.taskDetails.user = $("select[name='task_assignee']").val().join(",");

            /* Call to a task service function which will save new task in the database */
            this._taskService.add_new_task(this.taskDetails).subscribe(
                response => {
                    if (response.status) {
                        $("#taskModal").modal("hide");
                        this.create_task(response.task);
                        $("body").find("._uim_add_task_tags").val("");
                    }
                    else {
                        if (typeof response.errors === "object") {
                            for (var errorKey in response.errors) {
                                this._commonService.showAjaxErrors(response.errors[errorKey][0]);
                            }
                        }
                        else {
                            this._commonService.showAjaxErrors(response.message);
                        }
                    }
                },
                error => console.log(error),
            );

        }
    }

    create_task(taskDetails: any) {

        /* Kanban fucntion add item to add new task in frontend on status board */
        $('#taskKanBan').jqxKanban('addItem', {
            status: "data_" + $(".jqx-kanban-column:first").attr("data-column-data-field").split("_")[1].toString(),
            text: taskDetails.name,
            tags: (taskDetails.tag_name == null) ? " " : taskDetails.tag_name,
            color: "#5dc3f0",
            content: JSON.stringify({
                description: taskDetails.description,
                endDate: (taskDetails.end_date == null) ? "" : taskDetails.end_date,
                createdBy: taskDetails.user.first_name + " " + taskDetails.user.last_name,
                createdAt: taskDetails.created_at,
                taskId: taskDetails.id,
            }),
            className: (taskDetails.tag_name == null) ? "task_" + taskDetails.id.toString() + " _no_tags_task" + " order_" + taskDetails.order : "task_" + taskDetails.id.toString() + " order_" + taskDetails.order,	/* Custom class is added to get task id when updation is done over it */

        });
        this.tasks[taskDetails.id] = {
            id: taskDetails.toString(),
            state: "data_" + taskDetails.status.toString(),
            label: taskDetails.name,
            tags: taskDetails.tag_name,
            className: (taskDetails.tag_name == null) ? "task_" + taskDetails.id.toString() + " _no_tags_task" + " order_" + taskDetails.order : "task_" + taskDetails.id.toString() + " order_" + taskDetails.order,
            hex: "#5dc3f0",
            createdBy: taskDetails.created_by,
            content: JSON.stringify({
                description: taskDetails.description,
                endDate: (taskDetails.end_date == null) ? "" : taskDetails.end_date,
                createdBy: taskDetails.user.first_name + " " + taskDetails.user.last_name,
                createdAt: taskDetails.created_at,
                taskId: taskDetails.id,
            }),
            resourceId: taskDetails.id,
        }
        $("body").find("._no_tags_task").find(".jqx-kanban-item-footer").css("display", "none");

    }

    /* Function to add new status board */
    add_board() {
        /* Call to a task service function that will save new status board in database */
        this.statusBoardDetails.projectId = $("body").find("._project_id").val();
        this._taskService.add_new_status_board(this.statusBoardDetails).subscribe(
            response => this.create_status_board(response.data),
            error => console.log(error),
        );
    }

    create_status_board(statusBoard: any) {

        /* Get columns in the kanban then push newly added board details in kanban column and refresh kanban */
        let id = statusBoard.id.toString();
        let dataFieldValue = "";
        /* Condition to check if its first status board then set its dataField to data_1 */
        if (this.allStatus.length === 0) {
            dataFieldValue = "data_1";
        }
        else {
            dataFieldValue = "data_" + id;
        }

        this.allStatus.push({
            text: "" + statusBoard.status_name + "",
            iconClassName: "fa _delete_status status_" + statusBoard.id,
            dataField: dataFieldValue,
            collapsible: false
        })

        this.taskSource = {localData: this.tasks, dataType: "array", dataFields: this.taskFields};

        var dataAdapter = new $.jqx.dataAdapter(this.taskSource);
        this.create_task_kanban(dataAdapter);
        $("#statusModal").modal("hide");

    }

    /* Function that will intialise kanban with specified tasks and status */
    create_task_kanban(dataAdapter: any) {
        $('#taskKanBan').jqxKanban('destroy');
        $("._task_kanban_container").append($("<div>", {
            id: "taskKanBan"
        }));
        let self = this;
        $('#taskKanBan').jqxKanban({
            width: '100%',
            height: ($('.main-content').height() - 10) + 'px',
            resources: this.resourcesAdapterFunc(),
            source: dataAdapter,
            columns: self.allStatus,
            template: "<div class='jqx-kanban-item' id=''>"
            + "<div style='float:right;' class='fa fa-pencil-square-o edit-task jqx-kanban-item-template-content'></div>"
            + "<div style='float:right;' class='fa fa-eye view-task jqx-kanban-item-template-content'></div>"
            + "<div class='jqx-kanban-item-id-div'><span class='jqx-kanban-item-id' style='margin-left:5px;'></span>"
            + "<span class='jqx-kanban-item-text'></span>"
            + "</div>"
            + "<div class='jqx-kanban-item-task-description' style='padding:5px;'></div>"
            + "<div style='display: block;' class='jqx-kanban-item-footer'></div>"
            + "</div>",
            itemRenderer: function (element, item, resource) {
                if (item.content != "New content" && item.content !== null) {
                    let content = $.parseJSON(item.content);
                    if (content.description && content.description !== null) {
                        $(element[0]).find(".jqx-kanban-item-task-description").html(content.description);
                    }
                    if (content.taskId && content.taskId !== null) {
                        $(element[0]).find(".jqx-kanban-item-id").html("#" + content.taskId);
                    }
                }
            }
        });
        $("body").find("._no_tags_task").find(".jqx-kanban-item-footer").css("display", "none");
        $("body").find("._no_access").find(".edit-task").remove();
        self.projectOptions = true;
        self.viewTaskDetailPopup();
        self._progressService.done();
    }

    viewTaskDetailPopup() {
        /* Check if task id exist in route then open task details modal */
        if (typeof this.router.url.split("/")[4] !== "undefined" && this.router.url.split("/")[4] !== "") {
            $("body").find(".task_" + this.router.url.split("/")[4]).find(".view-task").trigger("click");
        }
    }

    delete_status() {
        var deleteId = $("input[name='deleteBoardId']").val();
        this._taskService.delete_task($("input[name='deleteBoardId']").val()).subscribe(
            response => {
                if (response.status) {
                    this._commonService.showAjaxSuccess(response.message);
                    $("div[data-column-data-field='data_" + deleteId + "']").remove();
                }
                else {
                    this._commonService.showAjaxErrors(response.message);
                }
                $("#deleteModal").modal("hide");
            },
            error => console.log(error)
        );

    }

    check_project_creator(projectDetails) {
        this._userService.get_user().subscribe(
            response => {
                if (response.user.id == projectDetails.projectCreatedBy) {
                    this.projectCreator = true;
                } else {
                    this.projectCreator = false;
                }
            },
            error => {
                console.log(error)
            }
        )
    }

    /* Function to delete task and related task data from database */
    delete_task() {
        var deleteTaskId = $("body").find("input[name='taskId']").val();
        this._taskService.delete_task(deleteTaskId).subscribe(
            response => {

                if (response.status) {
                    var taskKanbanId = $("#taskKanBan").find(".task_" + deleteTaskId).attr("id").split("_")[1];
                    $("#taskKanBan").jqxKanban('removeItem', taskKanbanId);
                    $(".modal").modal("hide");

                    this._commonService.showAjaxSuccess(response.message);
                }
                else {
                    this._commonService.showAjaxErrors(response.message);
                }
                $("#deleteModal").modal("hide");
            },
            error => console.log(error)
        );
    }

}
