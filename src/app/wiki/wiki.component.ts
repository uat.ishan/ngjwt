import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { ProjectService } from '../services/project/project.service';
import { CommonService } from '../services/common/common.service';
import { CredentialService } from '../services/credential/credential.service';
import { WikiService } from '../services/wiki/wiki.service';
import { sharedService } from '../services/project/shared.service';
import { Location } from '@angular/common';

@Component({
	selector: 'app-wiki',
	templateUrl: './wiki.component.html',
	styleUrls: ['./wiki.component.css']
})

export class WikiComponent implements OnInit {

	public projectDetails = { projectId:0, projectName:"", projectHandle:"", projectCategory:"", projectStatus:0, privacyStatus:1, projectAssignee:"",projectCreatedBy:"" };
	
	@Output() 
	public type;

	public wikiDetails:string;
	public wiki= {project_id:0, data:""};
	public wikiStatus:boolean= true;
	constructor(private router:Router, private _location:Location, private _ss:sharedService, private _projectService:ProjectService, private _commonService:CommonService, private _credentialService:CredentialService, private _wikiService:WikiService) { 
		this.type = "wiki";
	}

	ngOnInit() {

		let self = this;
 		let handleName = self.router.url.split("/")[4];
		self.get_single_project(handleName);
		
	}

	/* Function to edit current wiki  */
	edit_wiki(){
		this.wikiStatus = false;
		// let br = '<br/>';
		this.wiki.data = this.wikiDetails.replace(/<br>/g, "\n"); 
	}

	/* Function to get current project details */
	get_single_project(handleName){
		this._projectService.get_single_project(handleName).subscribe(
			response=>{
				if(response.status){
					this._ss.set_project(JSON.stringify(response.project));
					this.projectDetails = {
						projectId:response.project.id,
						projectName:response.project.name,
						projectHandle:response.project.handle,
						projectCategory:response.project.category,
						projectStatus:response.project.status,
						privacyStatus:response.project.privacy_status,
						projectAssignee:"",
						projectCreatedBy:response.project.created_by
					};

					/* After getting project details use projectId to get project details from wiki */
					this.get_project_wiki(response.project.id);
				}
				else{
					if(typeof response.errors === "object" ){
						for(var errorKey in response.errors){
							this._commonService.showAjaxErrors(response.errors[errorKey][0]);	
						}
					}
					else{
						this._commonService.showAjaxErrors(response.message);
					}
				}
			},
			error=>{
				console.log(error);
			}
		);
	}

	/* Function to get wiki of current project using projectId variable */
	get_project_wiki(projectId){
		if(projectId!=="" && projectId !== null){
			this._wikiService.get_project_wiki(projectId).subscribe(
				response =>{
					if(response.status){
						this.wikiDetails = response.wiki.data;
						this.wikiDetails = this.wikiDetails.replace(/\n/g, "<br>");
						this.wiki.data = response.wiki.data;
						this.wikiStatus = true;				
					} else {
						this.wikiStatus = false;
					}
				},
				error=>{ console.log(error) }
			);
		}
	}

	/* Function to add wiki details in the database */
	add_wiki(){
		if(this.wiki.data !== ""){
			this.wiki.project_id = this.projectDetails.projectId;
			this._wikiService.add_new_wiki(this.wiki).subscribe(
				response =>{
					this.wiki.data = response.wiki.data;
					this.wikiDetails = response.wiki.data.replace(/\n/g, "<br>");
					this.wikiStatus = true;
				},
				error=>{ console.log(error) }
			);
		} else {
			this._commonService.showAjaxErrors("Please fill wiki of project to save.");	
		}
	}


}
