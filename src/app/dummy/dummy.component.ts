import { Component, OnInit } from '@angular/core';
import { apiRoutes } from '../config/config';
declare var $: any;
declare var Dropzone: any;

@Component({
  selector: 'app-dummy',
  templateUrl: './dummy.component.html',
  styleUrls: ['./dummy.component.css']
})
export class DummyComponent implements OnInit {

	constructor() { }

	ngOnInit() {
		
		/* Autodiscover false prevents dropzone from showing error when page is refrersh after once intialisation of dropzpne*/
        Dropzone.autoDiscover = false;

        /* Dropzone function with required parameters */
        var userImage = new Dropzone("#userImage", { 
            url: "http://localhost/dropzone/index.php",
            // url: apiRoutes.user.updateUser,
            method:'post',
            maxFiles:1, 

            sending:function(file, xhr, formData){
               formData.append('someParameter[image]', file);
    			formData.append('someParameter[userName]', 'bob');
            },
            
            // addRemoveLinks:true,
            init: function() {
            	userImage = this;
                this.on("maxfilesexceeded", function(file) {
                    this.removeAllFiles();
                    this.addFile(file);
                });
                if (userImage.getQueuedFiles().length > 0) {
                	alert();
                 	userImage.processQueue(); 
                 	// Tell Dropzone to process all queued files.                    
                }
            },

            thumbnail: function(file, dataUrl) {
                $("._user_image").attr("src",dataUrl);
            },
            success:function(response){
                console.log(response);
            },
            error:function(error){
                console.log(error);
            }

        });		
	}

}
