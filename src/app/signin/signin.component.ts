import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Router} from '@angular/router'
import {AuthService} from '../services/auth/auth.service';
import {CommonService} from '../services/common/common.service';


@Component({
    selector: 'app-signin',
    templateUrl: './signin.component.html',
    styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

    constructor(private router: Router, private authServices: AuthService, private common: CommonService) {
        if(this.authServices.getUserLoggedIn()){
            this.router.navigate(['dashboard']);
        }
    }

    ngOnInit() {

    }

    onSignin(form: NgForm) {
       var self = this;
        this.authServices.signin(form.value.email, form.value.password)
            .subscribe(
            function (response) {
                if(response.status){
                    window.location.reload();
//                    self.router.navigate(['dashboard']);
                }else{
                    self.common.showAjaxErrors(response.message);
                }
            },
            function (error) {
                let data = $.parseJSON(error._body);
                if(data.message){
                    self.common.showAjaxErrors(data.message);
                }else{
                    // for debuging 
                    console.log(error.statusText);
                    self.common.showAjaxErrors('Something Went Wrong Please try again.');
                }
            },
            function () {
                // this code will run after subscription is completed
                console.log("the subscription is completed")
            });
    }

}
