import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { ProjectService } from '../services/project/project.service';
import { CommonService } from '../services/common/common.service';
import { CredentialService } from '../services/credential/credential.service';
import { sharedService } from '../services/project/shared.service';
import { Location } from '@angular/common';

@Component({
	selector: 'app-credential',
	templateUrl: './credential.component.html',
	styleUrls: ['./credential.component.css']
})

export class CredentialComponent implements OnInit {

  public projectDetails = { projectId:0, projectName:"", projectHandle:"", projectCategory:"", projectStatus:0, privacyStatus:1, projectAssignee:"",projectCreatedBy:"" };
	
	@Output() 
	public type;

	public credentialDetails:string;
	public credential= {project_id:0, data:""};
	public credentialStatus:boolean= true;
	constructor(private router:Router, private _location:Location, private _ss:sharedService, private _projectService:ProjectService, private _commonService:CommonService, private _credentialService:CredentialService) { 
		this.type = "credentials";
	}

	ngOnInit() {

		let self = this;
 		let handleName = self.router.url.split("/")[4];
		self.get_single_project(handleName);
		
	}

	edit_credential(){
		this.credentialStatus = false;
		this.credential.data = this.credentialDetails.replace(/<br>/g, "\n"); 
	}

	get_single_project(handleName){
		this._projectService.get_single_project(handleName).subscribe(
			response=>{
				if(response.status){
					this._ss.set_project(JSON.stringify(response.project));
					this.projectDetails = {
						projectId:response.project.id,
						projectName:response.project.name,
						projectHandle:response.project.handle,
						projectCategory:response.project.category,
						projectStatus:response.project.status,
						privacyStatus:response.project.privacy_status,
						projectAssignee:"",
						projectCreatedBy:response.project.created_by
					};
					this.get_project_credential(response.project.id);
				}
				else{
					if(typeof response.errors === "object" ){
						for(var errorKey in response.errors){
							this._commonService.showAjaxErrors(response.errors[errorKey][0]);	
						}
					}
					else{
						this._commonService.showAjaxErrors(response.message);
					}
				}
			},
			error=>{
				console.log(error);
			}
		);
	}

	get_project_credential(projectId){
		if(projectId!=="" && projectId !== null){
			this._credentialService.get_project_credential(projectId).subscribe(
				response =>{
					if(response.status){
						this.credential.data = response.credential.data;
						this.credentialDetails = response.credential.data.replace(/\n/g, "<br>");
						this.credentialStatus = true;				
					} else {
						this.credentialStatus = false;
					}
				},
				error=>{ console.log(error) }
			);
		}
	}

	add_credential(){
		if(this.credential.data !== ""){
			this.credential.project_id = this.projectDetails.projectId;
			this._credentialService.add_new_credential(this.credential).subscribe(
				response =>{
					this.credential.data = response.credential.data;
					this.credentialDetails = response.credential.data.replace(/\n/g, "<br>");
					this.credentialStatus = true;
				},
				error=>{ console.log(error) }
			);
		} else {
			this._commonService.showAjaxErrors("Please fill credential to save.");	
		}
	}

}
