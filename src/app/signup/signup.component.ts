import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms'; 
import { AuthService } from '../services/auth/auth.service';
import { CommonService } from '../services/common/common.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

    constructor(private router:Router, private _commonService: CommonService, private authService: AuthService) { }

    ngOnInit() {

    }

    onSignup(form: NgForm, event: Event){
    	event.preventDefault();
    	this.authService.signup(form.value).subscribe(
            response => {
                if(response.status){
                    this._commonService.showAjaxSuccess(response.message);
                    this.router.navigateByUrl("auth/signout");
                }
                else{
                    if (typeof response.errors === "object") {
                        for (var errorKey in response.errors) {
                            this._commonService.showAjaxErrors(response.errors[errorKey][0]);
                        }
                    }
                    else {
                        this._commonService.showAjaxErrors(response.message);
                    }
                }
            },	
            error => console.log(error),	
        );
    }

}
