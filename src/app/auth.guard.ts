import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {AuthService} from './services/auth/auth.service';
import {Router} from '@angular/router';
@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private authService: AuthService, private router:Router) {}
    canActivate( next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        if(!this.authService.getUserLoggedIn()){
//            window.layout = 'empty';
            this.router.navigate(['/']);
        }
        return this.authService.getUserLoggedIn();
    }
}
