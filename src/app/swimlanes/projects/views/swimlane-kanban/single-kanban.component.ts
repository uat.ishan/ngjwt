import {Component, OnInit} from '@angular/core';
import {CommonService} from '../../../../services/common/common.service';
import {ProjectService} from '../../../../services/project/project.service';
import {Router, ActivatedRoute, NavigationStart, NavigationEnd} from '@angular/router';
import {TaskService} from '../../../../services/task/task.service';
import {UserService} from '../../../../services/user/user.service';
import {NgProgressService} from 'ngx-progressbar';
import {sharedService} from '../../../../services/project/shared.service';
import {TaskSharedService} from '../../../../services/task/task.shared';
import { ActivityService } from '../../../../services/activity/activity.service';

declare var $: any;

@Component({
    selector: 'app-swimlane-kanban',
    templateUrl: './swimlane-kanban.component.html',
    styleUrls: ['./single-kanban.component.css']
})
export class SingleKanbanComponent implements OnInit {

    public userDetail = {id:"", firstName: "", lastName: "", email: "", password: "", confirm: "", new: ""};    
    public tasks = [];
    public allStatus = [];
    public taskFields = [];
    public taskSource: any;
    public users = [];
    public dataAdapter: any;
    public projectHandle: string;
    public taskDetails = {taskName: "", taskDescription: "", taskTags: "", taskId: "", status: "", user: "", projectId: 0, endDate: "", createdBy: "", createdAt: ""};

    constructor(private _activityService: ActivityService,public _taskShared: TaskSharedService, private _progressService: NgProgressService, private router: Router, private _taskService: TaskService, private _projectService: ProjectService, private _commonService: CommonService, private _userService: UserService, private _ss: sharedService) {
        this.taskFields = [
            {name: "id", type: "string"},
            {name: "status", map: "state", type: "string"},
            {name: "text", map: "label", type: "string"},
            {name: "tags", type: "string"},
            {name: "color", map: "hex", type: "string"},
            // {name: "resourceId", type: "number"},
            {name: "className", type: "string"},
            {name: "content", type: "string"}
        ];

        this.tasks[0] = {
            id: 0,
            state: "data_" + 0,
            label: "",
            tags: "",
            className: "task_" + 0,
            hex: "#5dc3f0",
            // resourceId: 0,
            content: "",
        };
    }

    ngOnInit() {
        var self = this;
        this.get_user();
    }

    get_user(){
        this._userService.get_user().subscribe(
            response => {
                if (response.status) {
                    this.userDetail.email = response.user.email;
                    this.userDetail.firstName = response.user.first_name;
                    this.userDetail.lastName = response.user.last_name;
                    this.userDetail.id = response.user.id;
                }
                else {

                }
                this._progressService.done();
            },
            error => {console.log(error);}
        );

    }


    /* Function that will intialise kanban with specified tasks and status */
    create_task_kanban(project: any, userDetail:any) {

        var self = this;

        /* Call to function that will create container for every project kanban */
        self.create_kanban_container(project,userDetail);

        /* Loop to set allStatus variable for Kanban */
        $.each(project.statuses, function (key, status) {
            self.set_status(status);
        });

        if (project.tasks.length > 0) {

            /* Loop to set task variable for Kanban */
            $.each(project.tasks, function (key, task) {
                self.set_tasks(task);
            });
        }

        /* jqxkanban variables are intialised below */
        self.taskSource = {localData: self.tasks, dataType: "array", dataFields: self.taskFields};
        self.dataAdapter = new (<any> $).jqx.dataAdapter(self.taskSource);
        self.projectHandle = project.handle;

        $('#taskKanBan_' + project.handle).jqxKanban({
            width: '100%',
            height: '400px',
            resources: this.resourcesAdapterFunc(),
            source: self.dataAdapter,
            columns: self.allStatus,
            template: "<div class='jqx-kanban-item' id=''>"
            + "<div style='float:right;' class='fa fa-pencil-square-o edit-task jqx-kanban-item-template-content'></div>"
            + "<div style='float:right;' class='fa fa-eye view-task jqx-kanban-item-template-content'></div>"
            // + "<div class='jqx-kanban-item-text'></div>"
             + "<div class='jqx-kanban-item-id-div'><span class='jqx-kanban-item-id' style='margin-left:5px;'></span>"
            + "<span class='jqx-kanban-item-text'></span>"
            + "</div>"
            + "<div class='jqx-kanban-item-task-description' style='padding:5px;'></div>"
            + "<div style='display: block;' class='jqx-kanban-item-footer'></div>"
            + "</div>",
            itemRenderer: function (element, item, resource) {
                if (item.content != "New content" && item.content !== null) {
                    let content = $.parseJSON(item.content);
                    if (content.description && content.description !== null) {
                        $(element[0]).find(".jqx-kanban-item-task-description").html(content.description);
                    }
                    if (content.taskId && content.taskId !== null) {
                        $(element[0]).find(".jqx-kanban-item-id").html("#"+content.taskId);
                    }
                }
            }
        });

        $("body").find("#taskKanBan_"+project.handle).find("._no_tags_task").find(".jqx-kanban-item-footer").css("display","none");

        $("body").on("click", "._project_name", function () {
            self.router.navigate(['/dashboard/project', $(this).attr("data-handle")]);
        });

        $("._project_manage").on("click",function(){
            self.router.navigate(['/dashboard/project/edit',$(this).attr("data-handle")]);
        });


        $("body").on("click", "._add_task_" + project.handle, function () {
            $("#taskModal").find("select[name='task_assignee']").val("").change()
            self._ss.set_project(JSON.stringify(project));
            $("#taskModal").modal("show");
        });

        $("._credential"+project.handle).on("click",function(){
            self.router.navigate(['/dashboard/project/credential',$(this).attr("data-handle")]);
        });

        $("._wiki"+project.handle).on("click",function(){
            self.router.navigate(['/dashboard/project/wiki',$(this).attr("data-handle")]);
        });

        $("body").find("._no_access").find(".edit-task").remove();

    }


    /* Function that will register in db activity when task is dragged from one status to other */
    register_task_activity(event){
        this.get_user();
        var taskId = parseInt(event.args.itemData.className.split("_")[1]);
        var activtyData = {
            source_id:taskId,
            type_id:"tasks",
            data:this.userDetail.firstName+" "+this.userDetail.lastName+" marked "+this.tasks[taskId].label+" as '"+event.args.newColumn.text+"'."
        }; 
        this._activityService.register_task_activity(activtyData).subscribe(
            response => {},
            error => {}
        );
    }

    /**
     * 
     * Function to add menu items in left menu in single kanban
     * @param data = {
            class:'testing',
            attr:{
                'data-id':23432
            },
            id:'this_is_id',
            html:'wiki',
            containerType:'<a>',
            href:'http://google.com',
            single_project_container:'.single_kanban_'+project.handle
        }
     * 
     */
    add_item_in_single_project_left_menu(data: any) {
        let html = $(data.containerType, {
            class: (data.class) ? data.class : '',
            id: (data.class) ? data.id : '',
            html: (data.html) ? data.html : '',
            style:"cursor:pointer;"
        });
        if (data.containerType == "<a>") {
            html.attr('href', data.href);
        }
        $.each(data.attr, function (name, value) {
            html.attr(name, value);
        });
        if(data.class !== "_project_name"){
            $(data.single_project_container).find('._project_name_div').append(" | ").append(html);    
        } else {
            $(data.single_project_container).find('._project_name_div').append(html);
        }
    }

    add_item_in_single_project_right_menu(data: any) {
        let html = $(data.containerType, {
            class: (data.class) ? data.class : '',
            id: (data.class) ? data.id : '',
            html: (data.html) ? data.html : '',
            style:"cursor:pointer;"
        });
        if (data.containerType == "<a>") {
            html.attr('href', data.href);
        }
        $.each(data.attr, function (name, value) {
            html.attr(name, value);
        });
        if($("._single_project_right_menu").children().length > 0 ){
            $(data.single_project_container).find('._single_project_right_menu').append(" | ").append(html);
        } else {
            $(data.single_project_container).find('._single_project_right_menu').append(" | ").append(html);
        }
    }

    create_kanban_container(project,user) {

        $("._task_kanban_container").append($("<div>", {
            class: 'single_kanban_container single_kanban_' + project.handle
        }).append($("<div>", {
            id: "taskKanBan_" + project.handle
        })));

        /* container that contain project name and manage project button on upper left corner */
        $("#taskKanBan_" + project.handle).before($('<div>', {
            'class': '_single_project_menus col-md-12'
        }).append($("<div>", {
            class: "col-md-7 _project_name_div"
        })));
        
        this.add_item_in_single_project_left_menu({
            class: '_project_name',
            attr: {
                'data-handle': project.handle
            },
            html: project.name,
            containerType: '<a>',
            single_project_container: '.single_kanban_' + project.handle
        });

        if(user.id == project.created_by){
            this.add_item_in_single_project_left_menu({
                class: '_project_manage',
                attr: {
                    'data-handle': project.handle
                },
                html: "<i class='fa fa-gear'></i>",
                containerType: '<a>',
                single_project_container: '.single_kanban_' + project.handle
            });
        }

        this.add_item_in_single_project_left_menu({
            class: '_credential'+project.handle,
            attr: {
                'data-handle': project.handle
            },
            html: 'Credentials',
            containerType: '<a>',
            single_project_container: '.single_kanban_' + project.handle
        });

        this.add_item_in_single_project_left_menu({
            class: '_wiki'+project.handle,
            attr: {
                'data-handle': project.handle
            },
            html: 'Wiki',
            containerType: '<a>',
            single_project_container: '.single_kanban_' + project.handle
        });

        /* container that contain refresh and add task button on upper right corner */
        $("#taskKanBan_" + project.handle).prev('._single_project_menus').append($("<div>", {
            class: "col-md-5 pull-right text-right _single_project_right_menu",
            'data-handle': project.handle,
            style: "cursor:pointer;"
        }).append($("<a>", {
            class: "_board_menu_item _add_task_" + project.handle,
            html: "Add Task"
        }).append($("<i>", {
            class: "fa fa-plus"
        }))));

    }

    set_status(status) {
        let dataFieldValue = "data_" + status.id;
        let id = status.id.toString();
        this.allStatus.push({
            text: status.status_name,
            iconClassName: "fa _delete_status status_" + id,
            dataField: dataFieldValue,
            collapsible: false
        });
    }

    set_tasks(task) {
        let taskClasses = (task.tag_name == null) ? "task_" + task.id.toString()+" _no_tags_task" : "task_" + task.id.toString();
        if(task.edit_check != 1){
            taskClasses += " _no_access"; 
        }

        this.tasks[task.id] = {
            id: task.id.toString(),
            state: "data_" + task.status.toString(),
            label: task.name,
            tags: (task.tag_name == null) ? " " : task.tag_name,
            className: taskClasses,
            hex: "#5dc3f0",
            // resourceId: task.id,
            createdBy:task.created_by,
            content: JSON.stringify({
                description: task.description,
                endDate: (task.end_date == null) ? "" : task.end_date,
                createdBy:task.user.first_name+" "+task.user.last_name,
                createdAt:task.created_at,
                taskId:task.id
            }),
        };
    }

    register_item_moved_event(handle: string) {
        var self = this;
        $('body').on('itemMoved', '#taskKanBan_' + handle, function (event) {

            /* variable that will contain current task id to update and status value to update */
            let taskData = {currentTaskId: "", newStatusId: "", taskOrder:{}};
            let args = event.args;
            let newColumn = args.newColumn;
            taskData.newStatusId = $("#" + newColumn.headerElement[0].id).find("._delete_status").attr("class").split(' ')[2].split("_")[1];

            var itemData = args.itemData;

            /* When new task is created at that time class is not added, but when new task is added it is possible to add class */
            if (typeof itemData.className.split("_")[1] !== "undefined") {
                taskData.currentTaskId = itemData.className.split("_")[1];
            }
            else if (typeof itemData.id !== "undefined") {
                taskData.currentTaskId = itemData.id;
            }

            self.register_task_activity(event);

            let taskOrder = self.get_task_order(taskData.newStatusId);
            taskData.taskOrder = taskOrder; 

            self._taskService.update_task_status(taskData).subscribe(
                response => {
                    if (response.status) {

                    }
                    else {
                        if (typeof response.errors === "object") {
                            for (var errorKey in response.errors) {
                                self._commonService.showAjaxErrors(response.errors[errorKey][0]);
                            }
                        }
                        else {
                            self._commonService.showAjaxErrors(response.message);
                        }

                    }
                },
                error => console.log(error),
            );
        });
    }

    /* function that will generate new task order */
    get_task_order(newStatusId){
        let orderObj = {};

        /* git all tasks in status board and create taskorder object and return */
        let tasks = $(".jqx-kanban-column[data-column-data-field='data_"+newStatusId+"']").find(".jqx-kanban-item");
        $.each(tasks,function(key,value){
            let taskId = $(value).attr("class").split(" ")[2].split("_")[1];
            let taskOrder = key+1;
            orderObj[key] = {
                "task_id":parseInt(taskId),
                "task_order":taskOrder,
            };
        });
        return orderObj;
    }


    register_item_edit_event(projectData,userDetail) {


        var data = $.parseJSON(projectData);
        var self = this;
        $('body').find('#taskKanBan_' + data.handle).on('click', '.edit-task', function () {

            /* set hidden field with projectid */
            $("body").find("input[name='projectId']").val("").val(data.id);

            /* Get task id from class 'task_' where id is set while getting all tasks */
            var taskId = {id:"",status:"edit"};
            taskId.id = $(this).parent(".jqx-kanban-item").attr("class").split("task_")[1].split(" ")[0];
            if(typeof self.tasks[taskId.id] !== "undefined" && $(this).parent("div").hasClass("_no_access")){
                self._commonService.showAjaxErrors("Access denied to edit task.");
                return false;
            }

            if (typeof self.tasks[taskId.id] !== "undefined") {
                let content = $.parseJSON(self.tasks[taskId.id].content);
                self.taskDetails.taskName = self.tasks[taskId.id].label;
                self.taskDetails.taskTags = self.tasks[taskId.id].tags;
                self.taskDetails.taskId = taskId.id;
                self.taskDetails.projectId = data.id;
                self.taskDetails.taskDescription = content.description;
                self.taskDetails.endDate = content.endDate;

                /* Call to shared task service function and current task value is transfered */
                self._taskShared.set_task(JSON.stringify(self.taskDetails));

                $("input[name='newTaskTags']").tagsinput('removeAll');
                $("input[name='newTaskTags']").tagsinput('add', self.tasks[taskId.id].tags);

                if (typeof self.taskDetails.endDate !== undefined)
                    $("#updateEndDate").datepicker('setDate', self.taskDetails.endDate.split(" ")[0]);

                /* Code to get single task details and set current assignees of task on asignees select2 */
                self.get_task_assignee(taskId)
                $("#updateTaskModal").modal("show");
            }
        });

    }

    register_item_view_event(projectData) {

        var self = this;
        var data = $.parseJSON(projectData);
        $('body').find("#taskKanBan_" + data.handle).on('click', '.view-task', function (args) {

            var taskId = {id:"",status:"show"};
            /* Get task id from class 'task_' where id is set while getting all tasks */
            taskId.id = $(this).parent(".jqx-kanban-item").attr("class").split("task_")[1].split(" ")[0];

            self.taskDetails.taskName = self.tasks[taskId.id].label;
            self.taskDetails.taskId = taskId.id;
            let content = $.parseJSON(self.tasks[taskId.id].content);
            self.taskDetails.taskDescription = content.description;
            self.taskDetails.endDate = content.endDate;

            if(typeof self.taskDetails.endDate == "undefined")
                self.taskDetails.endDate = "End date is not specified.";

            self.taskDetails.createdBy = content.createdBy;
            self.taskDetails.createdAt = content.createdAt;
            self.taskDetails.taskTags = self.tasks[taskId.id].tags;
            self.get_task_assignee(taskId);

            /* Call to shared project service function and current project value is transfered */
            self._ss.set_project(projectData);

        });

    }


    get_task_assignee(taskId) {
        this._taskService.get_single_task(taskId.id).subscribe(
            response => {
                this.taskDetails.user="";
                $(".update_task_assignee").val("");
                if (response.status) {
                    if (response.assigneeUserTask.assigneeUsers[0] != null) {
                        this.taskDetails.user = response.assigneeUserTask.assigneeUsers;
                        let asigneeId = [];

                        /* Loop to set make array of values and pass it in select2 to set selected value of task asignee */
                        for (let i = 0; i < response.assigneeUserTask.assigneeUsers.length; i++) {
                            asigneeId.push(response.assigneeUserTask.assigneeUsers[i].id);
                            $(".update_task_assignee").val(asigneeId);
                        }

                    }
                    if(taskId.status == "show")
                        $("#taskDetailModal").modal("show");
                    else
                        $("#updateTaskModal").modal("show");
                }
                /* Call to shared task service function and current task value is transfered */
                this._taskShared.set_task(JSON.stringify(this.taskDetails));
                $(".update_task_assignee").select2({placeholder: "Assignees"});
            },
            error => {}
        )
    }

    /* Function that will update task details */
    update_task_details() {

        /* Get tags values from taginput */
        this.taskDetails.taskTags = $("input[name='newTaskTags']").val();
        this.taskDetails.projectId = $("body").find("._project_id").val();
        this.taskDetails.endDate = $("#updateEndDate").val();
        if ($("body").find(".update_task_assignee").val() !== null)
            this.taskDetails.user = $("body").find(".update_task_assignee").val().join(",");

        this._taskService.update_task_details(this.taskDetails).subscribe(
            response => {
                if (response.status) {
                    $('#taskKanBan_' + this.projectHandle).jqxKanban('updateItem', response.task.id.toString(), {
                        text: response.task.name,
                        content: JSON.stringify({
                            description: response.task.description,
                            endDate: (response.task.end_date == null) ? " " : response.task.end_date,
                            createdBy:response.task.user.first_name+" "+response.task.user.last_name,
                            createdAt:response.task.created_at,
                        }),
                        tags: (response.task.tag_name == null) ? " " : response.task.tag_name,
                    });

                    if(response.task.tag_name != null){
                        $('#taskKanBan_' + this.projectHandle).find("._no_tags_task").removeClass("_no_tags_task");
                        $('#taskKanBan_' + this.projectHandle).find(".task_"+response.task.id).find(".jqx-kanban-item-footer").css("display","block");
                    } else if(response.task.tag_name == null){
                        $('#taskKanBan_' + this.projectHandle).find(".jqx-kanban-item").addClass("_no_tags_task");
                        $('#taskKanBan_' + this.projectHandle).find(".task_"+response.task.id).find(".jqx-kanban-item-footer").css("display","none");
                    }
                    
                    let taskClasses = (response.task.tag_name == null) ? "task_" + response.task.id.toString()+" _no_tags_task" : "task_" + response.task.id.toString();
                    if(response.task.edit_check != 1){
                        taskClasses += " _no_access"; 
                    }    


                    /* Update array that contains all task details */
                    this.tasks[this.taskDetails.taskId] = {
                        id: this.taskDetails.taskId.toString(),
                        state: "data_" + response.task.status.toString(),
                        label: response.task.name,
                        tags: (response.task.tag_name == null) ? " " : response.task.tag_name,
                        className: taskClasses,
                        hex: "#5dc3f0",
                        resourceId: response.task.id,
                        createdBy:response.task.created_by,
                        content: JSON.stringify({
                            description: response.task.description,
                            endDate: (response.task.end_date == null) ? " " : response.task.end_date,
                            createdBy:response.task.user.first_name+" "+response.task.user.last_name,
                            createdAt:response.task.created_at,
                            taskID:response.task.id
                        }),
                    }
                }
            },
            error => console.log(error),
        );
        $("#updateTaskModal").modal("hide");
    }

    create_task(taskDetails) {
        $('#taskKanBan_' + taskDetails.project.handle).jqxKanban('addItem', {
            status: "data_" + $('#taskKanBan_' + taskDetails.project.handle).find(".jqx-kanban-column:first").attr("data-column-data-field").split("_")[1].toString(),
            text: taskDetails.name,
            tags: (taskDetails.tag_name == null) ? " " : taskDetails.tag_name,
            color: "#5dc3f0",
            content: JSON.stringify({
                description: taskDetails.description,
                endDate: (taskDetails.end_date == null) ? "" : taskDetails.end_date,
                createdBy:taskDetails.user.first_name+" "+taskDetails.user.last_name,
                createdAt:taskDetails.created_at,
                taskId:taskDetails.id
            }),
            className: (taskDetails.tag_name == null) ? "task_" + taskDetails.id.toString()+" _no_tags_task" : "task_" + taskDetails.id.toString(),    /* Custom class is added to get task id when updation is done over it */
        });
        if(taskDetails.tag_name == null)
            $("body").find("#taskKanBan_"+taskDetails.project.handle).find("._no_tags_task").find(".jqx-kanban-item-footer").css("display","none");

        let taskClasses = (taskDetails.tag_name == null) ? "task_" + taskDetails.id.toString()+" _no_tags_task" : "task_" + taskDetails.id.toString();
        if(taskDetails.edit_check != 1){
            taskClasses += " _no_access"; 
        }

        this.tasks[taskDetails.id] = {
            id: taskDetails.toString(),
            state: "data_" + taskDetails.status.toString(),
            label: taskDetails.name,
            tags: taskDetails.tag_name,
            className: taskClasses,
            hex: "#5dc3f0",
            createdBy:taskDetails.created_by,
            content: JSON.stringify({
                description: taskDetails.description,
                endDate: (taskDetails.end_date == null) ? "" : taskDetails.end_date,
                createdBy:taskDetails.user.first_name+" "+taskDetails.user.last_name,
                createdAt:taskDetails.created_at,
                taskID:taskDetails.id
            }),
            resourceId: taskDetails.id,
        }
    }

    resourcesAdapterFunc() {

        var resourcesSource = {
            localData: [
                {id: 0},
            ],
            dataType: "array",
            dataFields: [
                {name: "id", type: "number"}
            ]
        };
        var resourcesDataAdapter = new $.jqx.dataAdapter(resourcesSource);
        return resourcesDataAdapter;

    }

    remove_task(taskDetails:any) {
        var taskKanbanId = $('#taskKanBan_' + taskDetails.project.handle).find(".task_"+taskDetails.task.taskId).attr("id").split("_")[2];
        $("body").find('#taskKanBan_' + taskDetails.project.handle).jqxKanban('removeItem',taskKanbanId);

    }
}
