import { Component, OnInit, Output } from '@angular/core';
import { CommonService } from '../../../../services/common/common.service';
import { ProjectService } from '../../../../services/project/project.service';
import { SingleKanbanComponent } from './single-kanban.component';
import { Router, ActivatedRoute, NavigationStart, NavigationEnd } from '@angular/router';
import { TaskService } from '../../../../services/task/task.service';
import { UserService } from '../../../../services/user/user.service';
import { NgProgressService } from 'ngx-progressbar'; 
import { TaskSharedService } from '../../../../services/task/task.shared';
import { sharedService } from '../../../../services/project/shared.service';
import { ActivityService } from '../../../../services/activity/activity.service';

declare var $: any;


@Component({
	selector: 'app-swimlane-kanban',
	templateUrl: './swimlane-kanban.component.html',
	styleUrls: ['./swimlane-kanban.component.css'],
	providers:[SingleKanbanComponent]
})
export class SwimlaneKanbanComponent implements OnInit {

    @Output() 
    public type;

    public projects = [];
    public userDetail = {id:"", firstName: "", lastName: "", email: "", password: "", confirm: "", new: ""};    
    public taskDetails = {taskName: "", taskDescription: "", taskTags: "", taskId: "", status: "", user: "", projectId: 0,endDate:"",createdBy:"", createdAt:""};
    public singleTaskAssignee = [];
    public users = [];
    public taskSource: any;
    public projectDetails = { id: 0, name: "", handle: "", category: "", status: 0, created_by:""};
	public taskAssignees = { userId: 0, first_name: "" };
    public kanBanObj = {};
    
    public taskFields = [];

    constructor(private _activityService: ActivityService,private _taskShared:TaskSharedService, private _progressService: NgProgressService, private router: Router,private _taskService:TaskService, private _projectService: ProjectService, private _commonService: CommonService, private _userService:UserService,private _ss:sharedService) { 
    }

	ngOnInit() {

		var self = this;
        self.type="tasks";
        
        self.get_user();

        /* subscribe over an task observable in shared task service */
        self._taskShared.get_task().subscribe(
            task => {
                /* When task changes taskDetails variable will be updated */
                let taskData = $.parseJSON(task);
                this.taskDetails = Object.assign(this.taskDetails,taskData);
            }
        );
        
        /* subscribe over an project observable in shared project service */
        self._ss.get_project().subscribe(
            project => {
                this.taskDetails = {taskName: "", taskDescription: "", taskTags: "", taskId: "", status: "", user: "", projectId: 0,endDate:"",createdBy:"", createdAt:""};

                /* When task changes taskDetails variable will be updated */
                let projectData = $.parseJSON(project);
                this.projectDetails = Object.assign(this.projectDetails,projectData);
            }
        );

        /* Get all users */
        self.get_all_users();
        
        /* Function call to get all projects, statuses and tasks */
        self.get_all_projects();
        
	}

    get_user(){
        this._userService.get_user().subscribe(
            response => {
                if (response.status) {
                    this.userDetail.email = response.user.email;
                    this.userDetail.firstName = response.user.first_name;
                    this.userDetail.lastName = response.user.last_name;
                    this.userDetail.id = response.user.id;
                }
                else {

                }
                this._progressService.done();
            },
            error => {console.log(error);}
        );

    }

    ngAfterContentInit() {
        let date = new Date();
        $(".task_assignee").select2({placeholder: "Task Asignees", width: '100%'});
        $(".update_task_assignee").select2({placeholder: "Task Asignees", width: '100%'});
        $('.datepicker').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
        });
        $('#addEndDate').datepicker('setDate', new Date());
    }

    get_all_users(){

        /* API call to userservice to get all users */
        this._userService.get_users().subscribe(
            response => {
                if (response.status) {
                    this.users = response.users.slice(3, response.users.length);
                }
                else {
                    if (typeof response.errors === "object") {
                        for (var errorKey in response.errors) {
                            this._commonService.showAjaxErrors(response.errors[errorKey][0]);
                        }
                    }
                    else {
                        this._commonService.showAjaxErrors(response.message);
                    }
                }
            },
            error => {
                console.log(error);
            }
        );
    }

	get_all_projects() {
        let self = this;

        /* Call to service function that will get all projects and their respective status and tasks */
        this._projectService.get_projects().subscribe(
            response => {
                if (response.status) {
                        
                    $.each(response.projects, function(key,project){

                        /* Created object in loop to utilise same variable multiple time for every instance */
                        self.kanBanObj[key] = new SingleKanbanComponent(self._activityService,self._taskShared, self._progressService, self.router, self._taskService, self._projectService, self._commonService, self._userService, self._ss);
                        self.kanBanObj[key].create_task_kanban(project,self.userDetail);
                        self.kanBanObj[key].register_item_moved_event(project.handle);
                        self.kanBanObj[key].register_item_edit_event(JSON.stringify({handle:project.handle, id:project.id}),self.userDetail);
                        self.kanBanObj[key].register_item_view_event(JSON.stringify(project));
                    });
                }
            },
            error => {
                let self = this;
                if (error.status === 401) {
                    this._commonService.showAjaxErrors("Login Session expired");
                    setTimeout(function(){
                        self.router.navigateByUrl("auth/signout");
                    }, 2000);
                }
            },
        );
    }

    add_task(){
        if (this.taskDetails.taskName == "") {
            this._commonService.showAjaxErrors("Please enter title for task !!");
        }
        else {
            $(".taskModal").find("select[name='task_assignee']").val("").change()
            if ($("select[name='task_assignee']").val() !== null)
                this.taskDetails.user = $("select[name='task_assignee']").val().join(",");

            this.taskDetails.taskTags = $("._uim_add_task_tags").val();
            this.taskDetails.projectId = this.projectDetails.id;
            this.taskDetails.status = $("body").find("#taskKanBan_"+this.projectDetails.handle).find(".jqx-kanban-column:first").attr("data-column-data-field").split("_")[1].toString();
            this.taskDetails.endDate = $("#addEndDate").val();
            
            /* Call to a task service function which will save new task in the database */
            this._taskService.add_new_task(this.taskDetails).subscribe(
                response => {
                    if (response.status) {
                        $("#taskModal").modal("hide");
                        this.kanBanObj[this.projectDetails.id].create_task(response.task);
                        $("body").find("._uim_add_task_tags").val("");
                    }
                    else {
                        if (typeof response.errors === "object") {
                            for (var errorKey in response.errors) {
                                this._commonService.showAjaxErrors(response.errors[errorKey][0]);
                            }
                        }
                        else {
                            this._commonService.showAjaxErrors(response.message);
                        }
                    }
                },
                error => console.log(error),
            );
        }


    }

    update_task(){
    	
    	/* Get project id from hidden field to update task */
        let projectId = $("body").find("input[name='projectId']").val();
        this.kanBanObj[projectId].taskDetails = Object.assign(this.kanBanObj[projectId].taskDetails,this.taskDetails);
        this.kanBanObj[projectId].update_task_details();
    }
    
    /* Function to delete task and related task data from database */
    delete_task(){
        let self = this;
        let deleteTaskId = this.taskDetails.taskId;
        this._taskService.delete_task(deleteTaskId).subscribe(
            response => {
                if (response.status) {
                    console.log(self.projectDetails);
                    let taskDetails = { project:self.projectDetails, task:self.taskDetails}
                    console.log(taskDetails);
                    // $("body").find('#taskKanBan_' + taskDetails.project.handle).jqxKanban('removeItem',taskDetails.task.taskId);
                    this.kanBanObj[this.taskDetails.projectId].remove_task(taskDetails);
                    $(".modal").modal("hide");
                    this._commonService.showAjaxSuccess(response.message);
                }
                else {
                    this._commonService.showAjaxErrors(response.message);
                }
                $("#deleteModal").modal("hide");
            },
            error => console.log(error)
        );        
    }
}
