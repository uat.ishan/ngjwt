import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SwimlaneKanbanComponent } from './swimlane-kanban.component';

describe('SwimlaneKanbanComponent', () => {
  let component: SwimlaneKanbanComponent;
  let fixture: ComponentFixture<SwimlaneKanbanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SwimlaneKanbanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwimlaneKanbanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
