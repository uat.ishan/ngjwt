import {Component, OnInit} from '@angular/core';
import {NavItem, NavItemType} from './lbd/lbd.module';
import {Router, NavigationStart, NavigationEnd} from '@angular/router';
import {AuthService} from './services/auth/auth.service';
import {ProjectService} from './services/project/project.service';

declare var $: any;

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
    public navItems: NavItem[];
    public layout: string = "default";

    constructor(private router: Router, private authService: AuthService, private _projectService: ProjectService) {

        // emptyView array contains all the urls which need to be displayed in empty layout with only a header
        const emptyView = [
            '/auth/signin',
            '/auth/signup',
            '/auth/forgot-password',
            '/auth/reset-password',
            '/'
        ];
        const self = this;
        router.events.subscribe((val) => {
            if (val instanceof NavigationEnd) {
                const copy = Object.assign({}, val);
                // using split here to remove query string from url if exist
                const url = copy.url.split(/[?#]/)[0];
                if ($.inArray(url, emptyView) >= 0) {
                    self['layout'] = "empty";
                } else {
                    self['layout'] = "default";
                }
            }
        });
    }

    public ngOnInit(): void {

        this.createNavBar();
        
        /* Function that will create navbar */
        // if (this.authService.getToken()) {
        //     this._projectService.get_projects().subscribe(
        //         response=>{
        //             if(response.status){
        //                 this.navItems.push(
        //                     {
        //                         type: NavItemType.NavbarRight,
        //                         title: 'Swimlane',
        //                         dropdownItems: [
        //                             {
        //                                 title: 'Projects',
        //                                 routerLink:'/dashboard/projects'
        //                             },
        //                             {title: 'User'},
        //                         ]
        //                     }
        //                 )        
        //             }
        //         },
        //         error=>{}
        //     );
        // }
    }

    public createNavBar(){

        /* Array with navbar items */
        this.navItems = [
            {type: NavItemType.Sidebar, title: 'Dashboard', routerLink: 'dashboard', iconClass: 'pe-7s-graph'},
            {
                type: NavItemType.NavbarRight,
                title: 'Profile',
                routerLink: 'dashboard/user'
            },
            {
                type: NavItemType.DropDownProjects,
                title: 'Projects',
                dropdownItems: [
                    {title: '+ Add New'},
                ]
            },
            {
                                type: NavItemType.NavbarRight,
                                title: 'Swimlane',
                                dropdownItems: [
                                    {
                                        title: 'Projects',
                                        routerLink:'/dashboard/projects'
                                    },
                                    {title: 'User'},
                                ]
                            }
        ];
        if (this.authService.getToken()) {
            this.navItems.push({type: NavItemType.NavbarRight, title: '<i class="fa fa-sign-out" aria-hidden="true"></i>', routerLink: 'auth/signout'});
        } else {
            this.navItems.push({type: NavItemType.NavbarRight, title: 'Sign In', routerLink: 'auth/signin'});
            this.navItems.push({type: NavItemType.NavbarRight, title: 'Sign Up', routerLink: 'auth/signup'});
        }


    }
}
