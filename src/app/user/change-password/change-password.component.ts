import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {apiRoutes} from '../../config/config';
import {UserService} from '../../services/user/user.service';
import {CommonService} from '../../services/common/common.service';
import {NgProgressService} from 'ngx-progressbar';

import { Location } from '@angular/common';

@Component({
    selector: 'app-change-password',
    templateUrl: './change-password.component.html',
    styleUrls: ['./change-password.component.css']
})

export class ChangePasswordComponent implements OnInit {

    public user = {firstName: "", lastName: "", email: "", password: "", confirm: "", new: ""};
    public currentPasswordStatus: boolean = false;

    constructor(private _progressService: NgProgressService, private _location: Location, private _userService: UserService, private _commonService: CommonService, private router: Router) {}

    ngOnInit() {
        this._progressService.start();
        /* Api call to get current user data  */
        this._userService.get_user().subscribe(
            response => {
                if (response.status) {
                    this.user.email = response.user.email;
                    this.user.firstName = response.user.first_name;
                    this.user.lastName = response.user.last_name;
                }
                else {

                }
                this._progressService.done();
            },
            error => {console.log(error);}
        );
    }

    check_current_password() {
        if (this.user.password !== "") {
            this._progressService.start();
            /* Check with API that if entered password is correct or not */
            this._userService.check_user_password(this.user.password).subscribe(
                response => {
                    this._progressService.done();
                    if (response.status) {
                        /* set status varable true to hide current password field and show new andd confirm password field */
                        this.currentPasswordStatus = true;
                        return this.currentPasswordStatus;
                    }
                    else {
                        this._commonService.showAjaxErrors(response.message);
                    }
                }
            )
        }
        else {
            this._commonService.showAjaxErrors("Fill your password");
        }
    }

    change_password() {
        this._progressService.start();

        /* API call to change pasword of the current user */
        this._userService.change_password(this.user).subscribe(
            response => {
                if (response.status) {
                    this._commonService.showAjaxSuccess(response.message);
                    this.router.navigate(["/dashboard/user"]);
                }
                else {
                    if (typeof response.errors === "object") {
                        for (var errorKey in response.errors) {
                            this._commonService.showAjaxErrors(response.errors[errorKey][0]);
                        }
                    }
                    else {
                        this._commonService.showAjaxErrors(response.message);
                    }

                }
                this._progressService.done();
            }
        )
    }

    back_to_profile() {
        this._location.back();
    }

}
