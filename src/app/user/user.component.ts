import {Component, OnInit, trigger, state, style, transition, animate} from '@angular/core';
import { NavbarTitleService } from '../lbd/services/navbar-title.service';
import { NgProgressService } from 'ngx-progressbar';
import { User } from "../interfaces/user.interface";
import { UserService } from '../services/user/user.service';
import { CommonService } from '../services/common/common.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  animations: [
    trigger('carduserprofile', [
      state('*', style({
        '-ms-transform': 'translate3D(0px, 0px, 0px)',
        '-webkit-transform': 'translate3D(0px, 0px, 0px)',
        '-moz-transform': 'translate3D(0px, 0px, 0px)',
        '-o-transform': 'translate3D(0px, 0px, 0px)',
        transform: 'translate3D(0px, 0px, 0px)',
        opacity: 1
      })),
      transition('void => *', [
        style({opacity: 0,
          '-ms-transform': 'translate3D(0px, 150px, 0px)',
          '-webkit-transform': 'translate3D(0px, 150px, 0px)',
          '-moz-transform': 'translate3D(0px, 150px, 0px)',
          '-o-transform': 'translate3D(0px, 150px, 0px)',
          transform: 'translate3D(0px, 150px, 0px)',
        }),
        animate('0.3s 0s ease-out'),
      ])
    ]),
    trigger('cardprofile', [
      state('*', style({
        '-ms-transform': 'translate3D(0px, 0px, 0px)',
        '-webkit-transform': 'translate3D(0px, 0px, 0px)',
        '-moz-transform': 'translate3D(0px, 0px, 0px)',
        '-o-transform': 'translate3D(0px, 0px, 0px)',
        transform: 'translate3D(0px, 0px, 0px)',
        opacity: 1})),
      transition('void => *', [
        style({opacity: 0,
          '-ms-transform': 'translate3D(0px, 150px, 0px)',
          '-webkit-transform': 'translate3D(0px, 150px, 0px)',
          '-moz-transform': 'translate3D(0px, 150px, 0px)',
          '-o-transform': 'translate3D(0px, 150px, 0px)',
          transform: 'translate3D(0px, 150px, 0px)',
        }),
        animate('0.3s 0.25s ease-out')
      ])
    ])
  ]
})
export class UserComponent implements OnInit {
  public formData: any;
  public userAbout: string;
  public user:User;

  constructor(private _progressService:NgProgressService, private navbarTitleService: NavbarTitleService, private userService : UserService, private commonService : CommonService) {
    this.userAbout = '"Lamborghini Mercy <br>Your chick she so thirsty <br>I\'m in that two seat Lambo"';
    this.formData = {
      username: '',
      email: '',
      firstName: '',
      lastName: '',
    };
  }

  public fill_user_detail(userDetail){

    this.formData = {
      username: userDetail.first_name,
      email: userDetail.email,
      firstName: userDetail.first_name,
      lastName: userDetail.last_name,
    }; 
    this._progressService.done();
  }


  public ngOnInit() {
    this._progressService.start();
    this.navbarTitleService.updateTitle('User Profile');
    this.userService.get_user()
    .subscribe(
         response=>{
           this.fill_user_detail(response.user);
         },
       error=>{ console.log() }         
    );
  }

  public update_user_profile(){
    if(this.formData.username === ""){
      this.commonService.showAjaxErrors(" Please specify username for profile. ");
    }
    else if(this.formData.email === "" ){
      this.commonService.showAjaxErrors(" Please enter valid email address. ");
    }
    else if(this.formData.firstName === ""){
      this.commonService.showAjaxErrors("Please enter first name. ");
    }
    else{
      this._progressService.start();

      this.userService.update_user(this.formData).subscribe(
        response => {
          if(response.status){
            this.commonService.showAjaxSuccess(response.message);
          }
          else{
            this.commonService.showAjaxErrors(response.message);
          }
          this._progressService.done();
        }, 
        error => console.log(error),  
      );
    }
  }

  public onSubmit() {
    console.log('Submitting values', this.formData);
    this.userService.addUser(this.formData)
    .subscribe(
        (user) => {
          this.user = user;
        }
      );
  }
}
