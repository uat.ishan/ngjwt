import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user/user.service';
import { CommonService } from '../services/common/common.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

	public userDetails = {email:""};
	public resetLinkSent:boolean = false;

	constructor(private _userService:UserService, private _commonService:CommonService) {

	}

	ngOnInit() {
	}

	forgot_password(){
		if(this.userDetails.email !== ""){
			this._userService.forgot_password(this.userDetails).subscribe(
				response => {
					if(response.status){
						this.resetLinkSent = true;						
					}
				},
				error=>{
					console.log(error);
				}
			)
		} else {
			this._commonService.showAjaxErrors("Please enter email for reset password link.");
		}
	}

}
