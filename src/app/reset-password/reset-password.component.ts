import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Router} from '@angular/router'
import {AuthService} from '../services/auth/auth.service';
import {CommonService} from '../services/common/common.service';
import {UserService} from '../services/user/user.service';

declare var $:any;

@Component({
	selector: 'app-reset-password',
	templateUrl: './reset-password.component.html',
	styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

	public userDetails = {email:"",password:"",confirmPassword:""};
	constructor(private router: Router, private authServices: AuthService, private common: CommonService, private _userService: UserService) {
		this._userService.getEmailResetPassword(this.router.url.split("/")[3]).subscribe(
			response=>{
				if(!response.status){
					this.router.navigate(["auth/signin"]);
					this.common.showAjaxErrors(response.error);
				} else {
					this.userDetails.email = response.email;
				}
			},
			error=>{}
		)	
	}

	ngOnInit() {

	}       

	resetPasssword(){	
		if($("input[name='new_password']").val() == "" && $("input[name='confirm_password']").val() == ""){
			this.common.showAjaxErrors("Please fill all details to update password");
		} else if($("input[name='new_password']").val() !== $("input[name='confirm_password']").val()){
			this.common.showAjaxErrors("Please match your password with confirm password.");
		} else {
			this.userDetails.password = $("input[name='new_password']").val();
			this.userDetails.confirmPassword = $("input[name='confirm_password']").val();
			this._userService.reset_password(this.userDetails).subscribe(
				response=>{
					if(response.status){
						this.router.navigate(["auth/signin"]);
						this.common.showAjaxSuccess("New password set successfully.");
					} else {
						if(typeof response.errors === "object" ){
							for(var errorKey in response.errors){
								this.common.showAjaxErrors(response.errors[errorKey][0]);	
							}
						}
						else{
							this.common.showAjaxErrors(response.message);
						}
					}
				},
				error=>{}
			)			
		}
	}

}
