export interface User{
	id:number,
	first_name:string,
	last_name:string,
	email:string,
	password:string,
	confirmation_code:string,
	confirmed:string,
	remember_token:string,
	created_at:string,
	updated_at:string,
	deleted_at:string,
}