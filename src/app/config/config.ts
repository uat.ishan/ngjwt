//export var apiBaseUrl = 'http://localhost:8000';
export var apiBaseUrl = 'http://api.uimatic.com';
export var apiRoutes = {
    task: {
        addNewTask: apiBaseUrl + "/api/task/create",
        updateTaskStatus: apiBaseUrl + "/api/task/change/status",
        updateTaskDetails: apiBaseUrl + "/api/task/",
        getAllTasks: apiBaseUrl + "/api/tasks/",
        deleteTask: apiBaseUrl + "/api/task/"
    },
    status: {
        addNewStatus: apiBaseUrl + "/api/status/create",
        getAllStatus: apiBaseUrl + "/api/project/"
    },
    project: {
        addNewProject: apiBaseUrl + "/api/project/create",
        getAllProjects: apiBaseUrl + "/api/projects",
        getSingleProject: apiBaseUrl + "/api/project/",
        updateProject: apiBaseUrl + "/api/project/",
        deleteProject: apiBaseUrl + "/api/project/",
        deleteAssignee: apiBaseUrl + "/api/project/assignee/delete"
    },
    user: {
        addNewUser: apiBaseUrl + "/api/user",
        getAllUsers: apiBaseUrl + "/api/users",
        updateUser: apiBaseUrl + "/api/user/update",
        getSingleUser: apiBaseUrl + "/api/user/get",
        checkPassword: apiBaseUrl + "/api/user/check_password",
        changePassword: apiBaseUrl + "/api/user/change_password",
        forgotPassword: apiBaseUrl + "/api/user/password/email",
        resetPassword: apiBaseUrl + "/api/user/password/reset/",
        newPassword: apiBaseUrl + "/api/user/reset/password/"
    },
    auth: {
        logIn: apiBaseUrl + "/api/auth/sign-in",
        logOut: apiBaseUrl + "/api/auth/logout/",
        signUp: apiBaseUrl + "/api/user"
    },
    comment: {
        saveComment: apiBaseUrl + "/api/comment/create",
        getTaskComments: apiBaseUrl + "/api/comments/find"
    },
    credential: {
        saveCredential: apiBaseUrl + "/api/credential/save",
        getCredential: apiBaseUrl + "/api/credential/"
    },
    wiki: {
        saveWiki: apiBaseUrl + "/api/wiki/save",
        getWiki: apiBaseUrl + "/api/wiki/"
    },
    activity: {
        newTaskActivity: apiBaseUrl + "/api/activity/task/save",
    },
    gitCommit:{
        getLastCommit: apiBaseUrl+"/api/status/get-last-commit"
    }
}    