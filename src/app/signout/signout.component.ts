import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router'
import {AuthService} from '../services/auth/auth.service';
import {CommonService} from '../services/common/common.service';

@Component({
    selector: 'app-signout',
    templateUrl: './signout.component.html',
    styleUrls: ['./signout.component.css']
})
export class SignoutComponent implements OnInit {

    constructor(private router: Router, private authServices: AuthService, private common: CommonService) {
        
        
        // delete token only if token is available in localstorage
        if (this.authServices.getToken() !== null) {

            /* Api call to destroy token from backend */
            this.authServices.user_logout(this.authServices.getToken()).subscribe(
                response=>{},
                error=>{}
            )

            // deleting token using auth services
            this.authServices.deletToken();
        
            // check if token is successfully deleted
            if (this.authServices.getToken() === null) {
                window.location.reload();
            } else {
         
                // If token removal fails due to any reason. Send message to user to delete cookies and cache to logut manually.
                this.common.showAjaxErrors('Something went wrong please clear cache and cookies of browser to logout. We will fix it soon.');
                // this.router.navigate(['dashboard']);
            }
        }else{
            this.router.navigate(['dashboard']);
        }
    }

    ngOnInit() {
        
    }

}
