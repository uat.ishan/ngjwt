import { Output, Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { ProjectService } from '../../services/project/project.service';
import { CommonService } from '../../services/common/common.service';
import { sharedService } from '../../services/project/shared.service';
import { UserService } from '../../services/user/user.service';
import { Location } from '@angular/common';
import { NgProgressService } from 'ngx-progressbar';

declare var $: any;

@Component({
  selector: 'app-edit-project',
  templateUrl: './edit-project.component.html',
  styleUrls: ['./edit-project.component.css']
})

export class EditProjectComponent implements OnInit {

	@Output() 
    public type;
	public projectDetails = { projectId:0, projectName:"", projectHandle:"", projectCategory:"", projectStatus:0, privacyStatus:1, projectAssignee:"",projectCreatedBy:"",delete_status:false };
	public index:any;
	public users = [];
	public projectTaskAssignees = [];
	public projectAssignees = [];
	public assigneeDetails = {id:0, tableName:"", projectId:0};
	public currentAssigneeElement:any;
	@ViewChildren('projectAssignee') projectAssignee: QueryList<any>;

	constructor(private _ss:sharedService, private _progressService:NgProgressService, private _location: Location, private _commonService:CommonService, private router:Router, private _projectService:ProjectService, private _userService:UserService) { }

	ngOnInit() {
		this.type="projects";
		this.get_all_users();
	}

	ngAfterViewInit() {
		var self = this;

		self._progressService.start();
 		
 		let handleName = self.router.url.split("/")[4];
		self.get_single_project(handleName);
	}

	get_single_project(handleName){
		this._projectService.get_single_project(handleName).subscribe(
			response=>{
				if(response.status){
					this._ss.set_project(JSON.stringify(response.project));
					this.projectDetails = {
						projectId:response.project.id,
						projectName:response.project.name,
						projectHandle:response.project.handle,
						projectCategory:response.project.category,
						projectStatus:response.project.status,
						privacyStatus:response.project.privacy_status,
						projectAssignee:"",
						projectCreatedBy:response.project.created_by,
						delete_status:false
					};
					/* check if login user is creator of project than allow manage project option */
                    this.check_project_creator(this.projectDetails);

					if(response.assignees.length > 0){
						let assigneeId = [];
						this.projectAssignees = response.assignees;
                        $(".project_assignee").val("");

                        /* Loop to set make array of values and pass it in select2 to set selected value of task asignee */
                        for (let i in response.assignees) {
                            assigneeId.push(response.assignees[i].id);
                        }
				    	$("body").find(".project_assignee").val(assigneeId);
				    	$("body").find(".project_assignee").select2();
					}

                	if(response.availaibleAssignees.length > 0){
						this.users = response.availaibleAssignees.slice(3,response.availaibleAssignees.length);
					}

					if(response.taskAssignees.length > 0){
						this.projectTaskAssignees = response.taskAssignees;
					}
				}
				else{
					if(typeof response.errors === "object" ){
						for(var errorKey in response.errors){
							this._commonService.showAjaxErrors(response.errors[errorKey][0]);	
						}
					}
					else{
						this._commonService.showAjaxErrors(response.message);
					}
				}
				this._progressService.done();	
			},
			error=>{
				console.log(error);
			}
		);
	}

	ngAfterContentInit() {
        $(".project_assignee").select2({placeholder: "Project Asignees", width: '100%'});
    }

	get_all_users(){
		this._userService.get_users().subscribe(
			response => {
				this.users = response.users.slice(3,response.users.length);
			},	
			error => { console.log(error); },
		)
	}

	/* Function to update project details */
	update_project(){
		if(this.projectDetails.projectName===""){
			this._commonService.showAjaxErrors("Please Enter Name For Project !!");
		}
		else if(this.projectDetails.projectHandle===""){
			this._commonService.showAjaxErrors("Please Enter handle Of Project !!");
		}
		else{
			this._progressService.start();

			this.projectDetails.privacyStatus = $("input[name='visibility']:checked").val();
	        
	        if ($("body").find(".project_assignee").val().length > 0){
	            this.projectDetails.projectAssignee = $("body").find(".project_assignee").val().join(",");
	        }
			else{
				this.projectDetails.projectAssignee= "";
				console.log($("body").find(".project_assignee").val());
			}


			/* call to API function that will udpate passed projectDetails */
			this._projectService.update_project(this.projectDetails).subscribe(
				response=>{
					if(response.status){
						this.projectDetails.privacyStatus = response.project.privacy_status;
						this.projectAssignees = response.assignees;
						console.log(this.projectAssignees);
						/* Call to functtion of shared service that will update project details */
						this._ss.set_project(JSON.stringify({name:response.project.name, handle:response.project.handle, id:response.project.id}));
						
						this._commonService.showAjaxSuccess(response.message);
					}
					else{
						if(typeof response.errors === "object" ){
							for(var errorKey in response.errors){
								this._commonService.showAjaxErrors(response.errors[errorKey][0]);	
							}
						}
						else{
							this._commonService.showAjaxErrors(response.message);
						}
					}
					this._progressService.done();
				},
				error=>{
					console.log(error);
				}
			);
		}
	}

	check_privacy_status(status){
		if(status === this.projectDetails.privacyStatus)
			return true;
		else
			return false;
	}

	back_to_project(){
		this._location.back();
	}

	remove_assignee($event){
		this.currentAssigneeElement = $("#"+$event.target.attributes.id.value).parent();
		if($event.target.attributes.id.value.split('_')[0] == "task"){
			this.assigneeDetails.tableName = $event.target.attributes.id.value.split('_')[0];
			this.assigneeDetails.id = $event.target.attributes.id.value.split('_')[1];
		}
		else if($event.target.attributes.id.value.split('_')[0] == "project"){
			this.assigneeDetails.tableName = $event.target.attributes.id.value.split('_')[0];
			this.assigneeDetails.id = $event.target.attributes.id.value.split('_')[2];
		}
		this.assigneeDetails.projectId = this.projectDetails.projectId;
	}

	delete_assignee(){

		this._projectService.delete_assignee(this.assigneeDetails).subscribe(
			response => {
				if(response.status){
					this.currentAssigneeElement.remove();
					(<any>$(".modal")).modal("hide");
					console.log('newAssignee');
					/* Remove assignee from select 2 also */
					if(this.assigneeDetails.tableName=="project"){
						let currentAssignee = (<any>$("body").find(".project_assignee").val());
						
						let newAssignee = this.update_assignee_list(currentAssignee);
						console.log(newAssignee);
						(<any>$("body").find(".project_assignee").val(newAssignee));
						(<any>$("body").find(".project_assignee")).select2();
					}
				}
			}
		)
	}

	update_assignee_list(currentAssignee){
		let newAssignee = [];
		for(let id in currentAssignee ){
			if(currentAssignee[id] == this.assigneeDetails.id){
				let preAssignees = currentAssignee.slice(0,id);
				let nextId = parseInt(id)+1;
				let postAssignees = currentAssignee.slice(nextId,currentAssignee.length);
				newAssignee = $.merge(preAssignees,postAssignees);
			}
		}
		return newAssignee;
	}

	check_project_creator(projectDetails){
        this._userService.get_user().subscribe(
            response => {
                if(response.user.id !== projectDetails.projectCreatedBy){
                    this.router.navigate(['/dashboard']);
                }
            },
            error => {
                console.log(error)
            }
        )
    }

    delete_project(){
    	/* Call to API function that will delete project and related data from database */
    	this._projectService.delete_project(this.projectDetails.projectId).subscribe(
			response => {
				if(response.status){
					this.router.navigate(["/dashboard"]);
					this.projectDetails.delete_status = true;
					this._ss.set_project(JSON.stringify(this.projectDetails));
				} else {
					this._commonService.showAjaxErrors(response.message);
				}
			}
		)
    }


}
