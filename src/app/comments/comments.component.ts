import {Component, OnInit, Input} from '@angular/core';
import {CommentsService} from '../services/comments/comments.service';
import {sharedService} from '../services/project/shared.service';
import {TaskSharedService} from '../services/task/task.shared';
import {CommonService} from '../services/common/common.service';
import {NgProgressService} from 'ngx-progressbar';
import {days} from '../config/comments_activity';

declare var window:any;

@Component({
    selector: 'app-comments',
    templateUrl: './comments.component.html',
    styleUrls: ['./comments.component.css']
})

export class CommentsComponent implements OnInit {

    @Input()
    type: string;

    public activityStatus: boolean = true;
    public commentData = {type: "", userId: "", message: "", sourceID: 0};
    public taskData = {taskId: 0};
    public projectData = {id: 0};
    public comments = [];
    public commentStatus: boolean = true;

    constructor(private _progressService: NgProgressService, private _commentsService: CommentsService, private _ss: sharedService, private _taskShared: TaskSharedService, private _commonService: CommonService) {
        if(typeof window.once == 'undefined'){
            /* get current project from shared project service */
            this._taskShared.get_task().subscribe(
                (task) => {
                    /* When task changes taskDetails variable will be updated */
                    let taskData = $.parseJSON(task);
                    this.taskData = Object.assign(this.taskData, taskData);
                    this.get_comments(this.taskData.taskId);
                }
            );
            
             /* get current project from shared project service */
            this._ss.get_project().subscribe(
                project => {
                    this._progressService.start();
                    /* When task changes taskDetails variable will be updated */
                    let projectData = $.parseJSON(project);
                    this.projectData = Object.assign(this.taskData, projectData);
                    this.get_comments(this.projectData.id);
                }
            );
            window.once=true;
        }
      
    }

    ngOnInit() {}

    /* function that will show or hide activity container depending on activityStatus value */
    toggle_activity() {
        if (this.activityStatus)
            this.activityStatus = false;
        else
            this.activityStatus = true;
    }

    /* Function to get all comments of current type */
    get_comments(dataId: number) {
        var self = this;
        self.comments = [];
        let commentData = {type: this.type, dataId: dataId, days: days.comments};
        this._commentsService.get_comments(commentData).subscribe(
            response => {
                if (response.status) {
                    self.comments = [];
                    /* set commentstatuss true to show activity container */
                    self.commentStatus = true;
                    $('.activity').html("");

                    /* set loop on comments in response to set comments variable of class */
                    $.each(response.comments, function (key, comment) {
                        if (typeof comment.data !== "undefined") {
                            let data = JSON.parse(comment.data)
                            comment.data = data.data;
                        }
                        self.comments.push(comment);

                    });
                } else {
                    // self.commentStatus = false;
                    if (typeof response.errors === "object") {
                        for (var errorKey in response.errors) {
                            this._commonService.showAjaxErrors(response.errors[errorKey][0]);
                        }
                    }
                }
                self._progressService.done();
            },
            error => {}
        );
    }

    /* Function that will call API function to save comment in database */
    add_comment() {
        this.commentData.type = this.type;    
        if (this.type == "tasks")
            this.commentData.sourceID = this.taskData.taskId;
        else if (this.type == "credentials" || this.type == "wiki" || this.type == "projects")
            this.commentData.sourceID = this.projectData.id;

        this._commentsService.add_new_comment(this.commentData).subscribe(
            response => {
                console.log(response);
                if (response.status) {

                    this.create_comment_div(response.comment);
                    this.commentStatus = true;
                    $("._comment_message").val("");

                } else {
                    $(".activity").html("");
                    if (typeof response.errors === "object") {
                        for (var errorKey in response.errors) {
                            this._commonService.showAjaxErrors(response.errors[errorKey][0]);
                        }
                    }
                    else {
                        this._commonService.showAjaxErrors(response.message);
                    }
                }
            },
            error => {}
        );
    }

    /* Function that will create comment container at top of all comments */
    create_comment_div(commentData) {
        this.activityStatus = true;
        let cloneCommentDiv = $(".activity-hidden").clone();
        cloneCommentDiv.removeClass("activity-hidden").addClass("activity");
        cloneCommentDiv.find("._comments_user b").html("").html(commentData.user.first_name + " " + commentData.user.last_name);
        cloneCommentDiv.find("._comments").html("").html(commentData.message);
        cloneCommentDiv.find(".comment-date").html("").html(commentData.created_at);
        $("body").find(".activity-list").prepend(cloneCommentDiv);
    }

}
