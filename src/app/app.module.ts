import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { AgmCoreModule } from 'angular2-google-maps/core';
import { DropzoneModule } from 'ngx-dropzone-wrapper';
import { DropzoneConfigInterface } from 'ngx-dropzone-wrapper';

import { AppComponent } from './app.component';
import { FooterLayoutComponent } from './footer-layout/footer-layout.component';
import { LbdModule } from './lbd/lbd.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserComponent } from './user/user.component';
import { TableComponent } from './table/table.component';
import { TypographyComponent } from './typography/typography.component';
import { IconsComponent } from './icons/icons.component';
import { MapsComponent } from './maps/maps.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { NgProgressModule } from 'ngx-progressbar';


// All serivces goes here
import { UserService } from './services/user/user.service';
import { ActivityService } from './services/activity/activity.service';
import { AuthService } from './services/auth/auth.service';
import { CommonService } from './services/common/common.service';
import { ProjectService } from './services/project/project.service';
import { sharedService } from './services/project/shared.service';
import { TaskSharedService } from './services/task/task.shared';
import { TaskService } from './services/task/task.service';
import { CommentsService } from './services/comments/comments.service';
import { CredentialService } from './services/credential/credential.service';
import { WikiService } from './services/wiki/wiki.service';


import { SignupComponent } from './signup/signup.component';
import { SigninComponent } from './signin/signin.component';
import { AppLayoutComponent } from './app-layout/app-layout.component';
import { SignoutComponent } from './signout/signout.component';
import { ProjectsComponent } from './projects/projects.component';
import { KanbanComponent } from './tasks/views/kanban/kanban.component';
import { TaskComponent } from './tasks/task.component';
import { AuthGuard } from './auth.guard';
import { EditProjectComponent } from './projects/edit-project/edit-project.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ChangePasswordComponent } from './user/change-password/change-password.component';
import { SwimlaneKanbanComponent } from './swimlanes/projects/views/swimlane-kanban/swimlane-kanban.component';
import { CommentsComponent } from './comments/comments.component';
import { WikiComponent } from './wiki/wiki.component';
import { CredentialComponent } from './credential/credential.component';
import { KanbanViewComponent } from './tasks/views/kanban-view/kanban-view.component';
import { ProjectNavModule } from './tasks/views/kanban-view/project-nav/project-nav.module';
import { ResetPasswordComponent } from './reset-password/reset-password.component'


const appRoutes: Routes = [
    {path: 'maps', component: MapsComponent},
    {
        path: 'auth', component: AppLayoutComponent, children:
        [
            {path: 'signup', component: SignupComponent},
            {path: 'signin', component: SigninComponent},
            {path: 'signout', component: SignoutComponent},
            {path: 'forgot-password', component: ForgotPasswordComponent},
            {path: 'reset-password/:token', component: ResetPasswordComponent},
        ]
    },
    {
        path: 'dashboard', component: FooterLayoutComponent, children:
        [
            {path: '', component: DashboardComponent, canActivate: [AuthGuard]},
            {path: 'projects', component: SwimlaneKanbanComponent, canActivate: [AuthGuard]},
            {path: 'user', component: UserComponent, canActivate: [AuthGuard]},
            {path: 'user/change-password', component: ChangePasswordComponent, canActivate: [AuthGuard]},
            {path: 'table', component: TableComponent, canActivate: [AuthGuard]},
            {path: 'typography', component: TypographyComponent, canActivate: [AuthGuard]},
            {path: 'icons', component: IconsComponent, canActivate: [AuthGuard]},
            {path: 'notifications', component: NotificationsComponent, canActivate: [AuthGuard]},
            {path: 'project/:handle_name', component: KanbanComponent, canActivate: [AuthGuard]},
            {path: 'project_test/:handle_name', component: KanbanViewComponent, canActivate: [AuthGuard]},
            {path: 'project/edit/:handle_name', component: EditProjectComponent, canActivate: [AuthGuard]},
            {path: 'project/wiki/:handle_name', component: WikiComponent, canActivate: [AuthGuard]},
            {path: 'project/credential/:handle_name', component: CredentialComponent, canActivate: [AuthGuard]},
            {path: 'project/:handle_name/:task_id', component: KanbanComponent, canActivate: [AuthGuard]},
        ]
    },
    {path: '', component: SigninComponent},
    {path: '**', redirectTo: 'auth/signin'}
];



@NgModule({
    declarations: [
        AppComponent,
        FooterLayoutComponent,
        DashboardComponent,
        UserComponent,
        TableComponent,
        TypographyComponent,
        IconsComponent,
        MapsComponent,
        NotificationsComponent,
        SignupComponent,
        SigninComponent,
        AppLayoutComponent,
        SignoutComponent,
        ProjectsComponent,
        KanbanComponent,
        TaskComponent,
        EditProjectComponent,
        ForgotPasswordComponent,
        ChangePasswordComponent,
        SwimlaneKanbanComponent,
        CommentsComponent,
        WikiComponent,
        CredentialComponent,
        KanbanViewComponent,
        ResetPasswordComponent,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        RouterModule.forRoot(appRoutes),
        AgmCoreModule.forRoot({apiKey: 'AIzaSyAEPDOJl5CPLz6NZcMqJBqZWfVXec3UsJg'}),
        LbdModule,
        NgProgressModule,
        ProjectNavModule
    ],
    providers: [
        UserService,
        ActivityService,
        AuthService,
        CommonService,
        ProjectService,
        TaskService,
        AuthGuard,
        CommentsService,
        sharedService,
        TaskSharedService,
        CredentialService,
        WikiService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
