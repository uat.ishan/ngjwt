import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GitStatusComponent } from './git-status.component';

describe('GitStatusComponent', () => {
  let component: GitStatusComponent;
  let fixture: ComponentFixture<GitStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GitStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GitStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
