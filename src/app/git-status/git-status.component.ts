import {Component, OnInit, Input, EventEmitter} from '@angular/core';
import {apiRoutes} from '../config/config';
import {Http, Response, Headers} from '@angular/http';
import { AuthService } from '../services/auth/auth.service';
@Component({
    selector: 'git-status',
    templateUrl: './git-status.component.html',
    styleUrls: ['./git-status.component.css']
})
export class GitStatusComponent implements OnInit {
	
	@Input() public commitId;

    constructor(private http: Http, private authService: AuthService) {}

    ngOnInit() {
    	console.log(this.commitId);
    }

}
